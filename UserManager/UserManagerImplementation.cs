﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Events.Types;
using CAALHP.Events.UserServiceEvents;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using FI.Events;
using FS.Events;
using UserManager.EventAggregators;
using UserManager.EventMessages;
using Caliburn.Micro;
using Event = CAALHP.Events.Event;
using RequestTypes = CAALHP.Events.Types.RequestTypes;

namespace UserManager
{
    public class UserManagerImplementation : IAppCAALHPContract, IHandle<BaseMessage>
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private const string Appname = "UserManager";

        public UserManagerImplementation()
        {
            ViewModelEventAggregator.EventAggregator.Subscribe(this);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(GetName()))
            {
                //Show homescreen
                Show();
            }
        }

        private void HandleEvent(UserListResponseEvent e)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new UserListResponseMessage()
                {
                    Sender = GetType(),
                    Users = e.Users,
                    PhotoRoot = e.PhotoRoot
                });
        }


        private void HandleEvent(UserUpdateCompleteEvent e)
        {
            RequestUserListFromCaalhp();
        }
        //private void HandleEvent(NewWebcamImageEvent e)
        //{
        //    ViewModelEventAggregator.EventAggregator.Publish(new NewWebcamImageMessage()
        //    {
        //        Sender = GetType(),
        //        Image = e.Image
        //    });

        //}

        private void HandleEvent(BiometricsResponsEvent e)
        {
            switch (e.Type)
            {
                case BiometricDeviceType.Facial:
                    Utils.FacialDriverEnabled = true;
                    break;
                case BiometricDeviceType.Finger:
                    Utils.FingerDriverEnabled = true;
                    break;
                case BiometricDeviceType.Voice:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            ViewModelEventAggregator.EventAggregator.Publish(new UpdateBiometricsMessage()
            {
                Sender = GetType()
            });


        }
        private void HandleEvent(EnrollUserErrorResponsEvent e)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new EnrollErrorResponsMessage()
            {
                Sender = GetType(),
                Error = e.Error
            });

            //ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage()
            //{
            //    Sender = GetType(),
            //    Message = e.Error
            //});

        }

        private void HandleEvent(EnrollUserResponsEvent e)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new EnrollResponsMessage()
            {
                Sender = GetType(),
                UserId = e.UserId,
                Template = e.Template
            });
        }

        private void HandleEvent(FingerEnrollResponsEvent e)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new FingerEnrollResponsMessage()
            {
                Sender = GetType(),
                UserId = e.UserId,
                Template = e.Template
            });
        }

        private void HandleEvent(FingerEnrollErrorResponsEvent e)
        {

            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage()
            {
                Sender = GetType(),
                Message = e.Error
            });
        }

        private void HandleEvent(UserLoggedInEvent e)
        {

            ViewModelEventAggregator.EventAggregator.Publish(new UserLoggedInMessage()
            {
                Sender = GetType(),
                User = e.User
            });
        }

        private void HandleEvent(UserLoggedOutEvent e)
        {

            ViewModelEventAggregator.EventAggregator.Publish(new UserLoggedOutMessage()
            {
                Sender = GetType()
            });
        }


        public string GetName()
        {
            return Appname;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Application.Current.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;

            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedInEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedOutEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserListResponseEvent)), _processId);
            //  _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(NewWebcamImageEvent), "FI.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(EnrollUserErrorResponsEvent), "FI.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(EnrollUserResponsEvent), "FI.Events"), _processId);

            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FingerEnrollResponsEvent), "FS.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FingerEnrollErrorResponsEvent), "FS.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(BiometricsResponsEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserUpdateCompleteEvent)), _processId);

            RequestUserListFromCaalhp();
            RequestBiometricsAvaileble();

        }

        private void RequestBiometricsAvaileble()
        {
            var reqEvent = new BiometricsRequestEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, reqEvent);
            if (_host != null)
                _host.Host.ReportEvent(serializedEvent);
        }

        private void RequestUserListFromCaalhp()
        {
            var reqEvent = new UserListRequestEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId
                };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, reqEvent);
            if (_host != null)
                _host.Host.ReportEvent(serializedEvent);
        }

        public void Show()
        {
            RequestUserListFromCaalhp();
            ViewModelEventAggregator.EventAggregator.Publish(new ShowMessage { Sender = GetType() });
        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
        }

        private void HandleMessage(BaseMessage o)
        {
            //this function is by design empty.
        }

        private void HandleMessage(RequestFacialEnrollMessage o)
        {
            var reqEvent = new EnrollUserRequestEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                UserId = o.UserId
            };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, reqEvent, "FI.Events");
            if (_host != null)
                _host.Host.ReportEvent(serializedEvent);
        }

        private void HandleMessage(RequestUserListMessage o)
        {
            //Debugger.Launch();

            RequestUserListFromCaalhp();
        }



        private void HandleMessage(RequestFingerEnrollMessage o)
        {
            var request = new FingerEnrollRequestEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                UserId = o.UserId,
            };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, request, "FS.Events");
            if (_host != null)
                _host.Host.ReportEvent(serializedEvent);
        }

        //private void HandleMessage(RequestFingerRemoveMessage o)
        //{
        //    var request = new Finger
        //    {
        //        CallerName = GetName(),
        //        CallerProcessId = _processId,
        //        UserId = o.UserId,
        //    };

        //    var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, request, "FS.Events");
        //    _host.Host.ReportEvent(serializedEvent);
        //}

        private void HandleMessage(RequestFacialRemoveMessage o)
        {

            var request = new DeleteUserRequestEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    UserId = o.UserId,
                };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, request, "FI.Events");
            if (_host != null)
                _host.Host.ReportEvent(serializedEvent);
        }

        private void HandleMessage(RequestFingerRemoveMessage o)
        {
            var request = new FingerDeleteUserRequestEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                UserId = o.UserId,
            };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, request, "FS.Events");
            if (_host != null)
                _host.Host.ReportEvent(serializedEvent);
        }


        private void HandleMessage(UserToEditCompleteMessage msg)
        {
            var editedUser = msg.User;
            var userToSend = new UserProfile()
                {
                    CreationTime = editedUser.CreationTime,
                    UserId = editedUser.Id,
                    UserLevel = editedUser.UserLevel,
                    DateOfBirth = editedUser.DateOfBirth,
                    Email = editedUser.Email,
                    Height = editedUser.Height,
                    MobileNumber = editedUser.MobileNumber,
                    FullName = editedUser.Name,
                    Pin = editedUser.Pin,
                    FacialProfile = editedUser.FacialTemplateExsists,
                    FingerPrint = editedUser.FingerTemplateExsists,
                    HasPhoto = editedUser.HasProfilePhoto,
                    UserName = editedUser.Name
                };

            var updateUserEvent = new CreateUserLocalEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    UserProfile = userToSend,
                    NewPhoto = msg.NewPhoto,
                    PhotoRoot = msg.PhotoRoot
                };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, updateUserEvent);
            if (_host != null)
                _host.Host.ReportEvent(serializedEvent);
        }

        private void HandleMessage(NewUserMessage msg)
        {
            var editedUser = msg.NewUser;
            var userToSend = new UserProfile()
            {
                CreationTime = editedUser.CreationTime,
                UserId = editedUser.Id,
                UserLevel = editedUser.UserLevel,
                DateOfBirth = editedUser.DateOfBirth,
                Email = editedUser.Email,
                Height = editedUser.Height,
                MobileNumber = editedUser.MobileNumber,
                FullName = editedUser.Name,
                Pin = editedUser.Pin,
                FacialProfile = editedUser.FacialTemplateExsists,
                FingerPrint = editedUser.FingerTemplateExsists,
                HasPhoto = editedUser.HasProfilePhoto,
                UserName = editedUser.Name
            };

            var updateUserEvent = new CreateUserLocalEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                UserProfile = userToSend,
                NewPhoto = editedUser.HasProfilePhoto,
                PhotoRoot = msg.PhotoRoot
            };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, updateUserEvent);
            if (_host != null)
                _host.Host.ReportEvent(serializedEvent);
        }




    }
}
