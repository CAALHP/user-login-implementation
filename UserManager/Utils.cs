﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UserManager
{
    public static class Utils
    {
        public static bool FingerDriverEnabled { get; set; }
        public static bool FacialDriverEnabled { get; set; }

        public static string ImagePath
        {
            get { return AssemblyDirectory + "\\Images\\"; }
        }

        public static string AssemblyDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public static bool GetUserImage(string photoLink, string sourcePath)
        {
            if (string.IsNullOrEmpty(photoLink))
                return false;

            try
            {
                var targetImgPath = Path.Combine(ImagePath, photoLink);
                var sourceImgPath = Path.Combine(sourcePath, photoLink);

                var result = CopyImagefromSource(sourceImgPath, targetImgPath);

                return result; //image = Image.FromFile(targetImgPath);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static bool CopyImagefromSource(string sourceImgPath, string targetImgPath)
        {
            if (!File.Exists(targetImgPath) ||
                File.GetLastWriteTime(targetImgPath) < File.GetLastWriteTime(sourceImgPath))
            {
                File.Copy(sourceImgPath, targetImgPath, true);
                return true;
            }

            return false;

        }
    }
}
