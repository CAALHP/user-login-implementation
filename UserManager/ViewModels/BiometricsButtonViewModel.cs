﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;
using UserManager.Model;

namespace UserManager.ViewModels
{
    [Export(typeof(BiometricsButtonViewModel))]
    public abstract class BiometricsButtonViewModel:BaseViewModel
    {
        private string _typeName;
        private string _icon;
        protected User User;
        private bool _isDeviceEnabled;
        private string _deviceStatus;
        private bool _userBiometricsExsists;
        private bool _enrollingRunning;

        protected BiometricsButtonViewModel(User u)
        {
            User = u;

            Template = null;
        }

        public bool EnrollingRunning
        {
            get { return _enrollingRunning; }
            set
            {
                
                _enrollingRunning = value;
                NotifyOfPropertyChange(() => EnrollEnable);
            }
        }

        public string TypeName
        {
            get { return _typeName; }
            set
            {
                if (value == _typeName) return;
                _typeName = value;
                NotifyOfPropertyChange(() => TypeName);
            }
        }

        public string Icon
        {
            get { return _icon; }
            set
            {
                if (Equals(value, _icon)) return;
                _icon = value;
                NotifyOfPropertyChange(() => Icon);
            }
        }

        public bool IsDeviceEnabled
        {
            get { return _isDeviceEnabled; }
            set
            {
                if (value.Equals(_isDeviceEnabled)) return;
                _isDeviceEnabled = value;
                NotifyOfPropertyChange(() => IsDeviceEnabled);
            }
        }

        public bool UserBiometricsExsists
        {
            get { return _userBiometricsExsists; }
            set
            {
                if (value.Equals(_userBiometricsExsists)) return;
                _userBiometricsExsists = value;
                NotifyOfPropertyChange(() => UserBiometricsExsists);
                NotifyOfPropertyChange(() => EnrollEnable);
            }
        }

        public string DeviceStatus
        {
            get { return _deviceStatus; }
            set
            {
                if (value == _deviceStatus) return;
                _deviceStatus = value;
                NotifyOfPropertyChange(() => DeviceStatus);
            }
        }

        public byte[] Template { get; protected set; }

        public BiometricDeviceType DeviceType { get; protected set; }

        public bool EnrollEnable
        {
             get
             {
                 return !UserBiometricsExsists && !EnrollingRunning;
             }
        }

        //public bool CanEnroll { 
        //    get
        //    {
        //        return string.IsNullOrEmpty(User.Name) && string.IsNullOrEmpty(User.DateOfBirth) &&
        //               string.IsNullOrEmpty(User.Email) && string.IsNullOrEmpty(User.Height);
        //    }
        //}

        public abstract void Enroll();
        public abstract void Dispose();
        public abstract void StoreTemplate(User userToEdit);
        public abstract void RemoveTemplate(User userToEdit);
    }
}
