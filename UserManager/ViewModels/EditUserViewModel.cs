﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using CAALHP.Events.Types;
using Caliburn.Micro;
using UserManager.EventAggregators;
using UserManager.EventMessages;

using UserManager.Model;
using UserManager.Properties;

namespace UserManager.ViewModels
{
    public class EditUserViewModel : BaseViewModel, IHandle<BaseMessage>
    {
        //private IDatasService _datasService = new DataService();

        private User _userToEdit;
        private string _date;
        private string _month;
        private string _year;
        private string _name;
        private string _email;
        private string _pin;
        private CareStoreUserLevel _userLevel;
        private string _dateOfBirth;
        private string _mobileNumber;
        private string _height;
        private bool _fingerPrintExsists;
        private bool _facialProfileExsists;
        private BitmapImage _webCamBitmapImage;
        private object _pictureLock = new object();
        private bool _takeProfilePicture;
        private readonly string _noPhotoPath = Utils.ImagePath + "PhotoIcon.png";
        private string _status;
        private List<BiometricsButtonViewModel> _biometrics1;
        private string _storeState;
        private BitmapImage _profileImage;
        private bool _storeRunning;


        public EditUserViewModel()
        {

            UserToEdit = new User();

            ViewModelEventAggregator.EventAggregator.Subscribe(this);

            Biometrics = new List<BiometricsButtonViewModel>
                {
                    new FacialBiometricsButtonViewModel(UserToEdit),
                    new FingerBiometricsButtonViewModel(UserToEdit)
                };

            StoreButtonStateText = "Create";

            SetProfileImage(_noPhotoPath);
            //var img = new BitmapImage();
            //img.BeginInit();
            //img.CacheOption = BitmapCacheOption.OnLoad;
            //img.UriSource = new Uri(_noPhotoPath, UriKind.Absolute);
            //img.EndInit();
            //ProfileImage = img;

        }

        public EditUserViewModel(User usertoEdit)
        {
            UserToEdit = usertoEdit;
            UpdateAllPropertiesFromUser();

            ViewModelEventAggregator.EventAggregator.Subscribe(this);


            Biometrics = new List<BiometricsButtonViewModel>
                {
                    new FacialBiometricsButtonViewModel(UserToEdit),
                    new FingerBiometricsButtonViewModel(UserToEdit)
                };

            StoreButtonStateText = "Update";


        }

        public void Dispose()
        {
            foreach (var biometricsButtonViewModel in Biometrics)
            {
                try
                {
                    biometricsButtonViewModel.Dispose();
                }
                catch (Exception)
                {

                    ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage()
                    {
                        Sender = GetType(),
                        Message = "Error while closing biometrics"
                    });
                }

            }

            UserToEdit = null;
        }


        public User UserToEdit
        {
            get { return _userToEdit; }
            set
            {
                if (Equals(value, _userToEdit)) return;
                _userToEdit = value;
                NotifyOfPropertyChange(() => UserToEdit);
            }
        }

        private void UpdateAllPropertiesFromUser()
        {
            Name = UserToEdit.Name;
            Height = UserToEdit.Height;
            UserLevel = UserToEdit.UserLevel;
            Email = UserToEdit.Email;
            Pin = UserToEdit.Pin;
            MobileNumber = UserToEdit.MobileNumber;


            var photoPath = File.Exists(Utils.ImagePath + UserToEdit.PhotoLink) ? Utils.ImagePath + UserToEdit.PhotoLink : _noPhotoPath;

            SetProfileImage(photoPath);

            var date = UserToEdit.DateOfBirth.Split('/');

            Date = date[0];
            Month = date[1];
            Year = date[2];

        }

        private void SetProfileImage(string photoPath)
        {
            var img = new BitmapImage();
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.UriSource = new Uri(photoPath, UriKind.Absolute);
            img.EndInit();
            ProfileImage = img;
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                NotifyOfPropertyChange(() => Name);
                NotifyStatus();
            }
        }

        public string DateOfBirth
        {
            get { return _dateOfBirth; }
            set
            {
                if (value == _dateOfBirth) return;
                _dateOfBirth = value;
                NotifyOfPropertyChange(() => DateOfBirth);
                NotifyStatus();
            }
        }

        public string Height
        {
            get { return _height; }
            set
            {
                if (value == _height) return;
                _height = value;
                NotifyOfPropertyChange(() => Height);
                NotifyStatus();
            }
        }

        public string Email
        {
            get { return _email; }
            set
            {
                if (value == _email) return;
                _email = value;
                NotifyOfPropertyChange(() => Email);
                NotifyStatus();
            }
        }

        public string MobileNumber
        {
            get { return _mobileNumber; }
            set
            {
                if (value == _mobileNumber) return;
                _mobileNumber = value;
                NotifyOfPropertyChange(() => MobileNumber);
                NotifyStatus();
            }
        }

        public BitmapImage ProfileImage
        {
            get { return _profileImage; }
            private set
            {
                if (Equals(value, _profileImage)) return;
                _profileImage = value;
                NotifyOfPropertyChange(() => ProfileImage);
            }
        }

        public string Pin
        {
            get { return _pin; }
            set
            {
                if (value == _pin) return;
                _pin = value;
                NotifyOfPropertyChange(() => Pin);
                NotifyStatus();
            }
        }

        public bool FingerPrint
        {
            get { return _fingerPrintExsists; }
            set
            {
                if (value.Equals(_fingerPrintExsists)) return;
                _fingerPrintExsists = value;
                NotifyOfPropertyChange(() => FingerPrint);
            }
        }

        public bool FacialProfile
        {
            get { return _facialProfileExsists; }
            set
            {
                if (value.Equals(_facialProfileExsists)) return;
                _facialProfileExsists = value;
                NotifyOfPropertyChange(() => FacialProfile);
            }
        }

        public CareStoreUserLevel UserLevel
        {
            get { return _userLevel; }
            set
            {
                if (value == _userLevel) return;
                _userLevel = value;
                NotifyOfPropertyChange(() => UserLevel);
                NotifyStatus();
            }
        }

        public CareStoreUserLevel SelectedEnumType
        {
            get { return UserLevel; }
            set
            {
                if (value == UserLevel) return;
                UserLevel = value;
                NotifyOfPropertyChange(() => SelectedEnumType);
            }
        }

        public IEnumerable<CareStoreUserLevel> CareStoreUserLevelsEnumTypeValues
        {
            get
            {
                return Enum.GetValues(typeof(CareStoreUserLevel))
                    .Cast<CareStoreUserLevel>();
            }
        }

        public string Date
        {
            get { return _date; }
            set
            {
                if (value == _date) return;
                _date = value;
                NotifyOfPropertyChange(() => Date);
            }
        }

        public string Month
        {
            get { return _month; }
            set
            {
                if (value == _month) return;
                _month = value;
                NotifyOfPropertyChange(() => Month);
            }
        }

        public string Year
        {
            get { return _year; }
            set
            {
                if (value == _year) return;
                _year = value;
                NotifyOfPropertyChange(() => Year);
            }
        }

        public BitmapImage WebCamBitmapImage
        {
            get { return _webCamBitmapImage; }
            set
            {
                if (Equals(value, _webCamBitmapImage)) return;
                _webCamBitmapImage = value;
                NotifyOfPropertyChange(() => WebCamBitmapImage);
            }
        }

        //TODO Improved status message, maybe move to shell like marketplace loading msg in lower right corner.
        public string Status
        {
            get { return _status; }
            set
            {
                if (value == _status) return;
                _status = value;
                NotifyOfPropertyChange(() => Status);
            }
        }

        public string StoreButtonStateText
        {
            get { return _storeState; }
            set
            {
                if (value == _storeState) return;
                _storeState = value;
                NotifyOfPropertyChange(() => StoreButtonStateText);
            }
        }

        public List<BiometricsButtonViewModel> Biometrics
        {
            get { return _biometrics1; }
            set
            {
                if (Equals(value, _biometrics1)) return;
                _biometrics1 = value;
                NotifyOfPropertyChange(() => Biometrics);
            }
        }

        private void NotifyStatus()
        {
            NotifyOfPropertyChange(() => CanEnroll);
            NotifyOfPropertyChange(() => CanStore);
        }

        public bool CanEnroll
        {
            get
            {
                var result = !string.IsNullOrEmpty(Name) && !string.IsNullOrEmpty(Date) && !string.IsNullOrEmpty(Month) && !string.IsNullOrEmpty(Year) &&
                       !string.IsNullOrEmpty(Email) && !string.IsNullOrEmpty(Height) && !string.IsNullOrEmpty(Pin);
                return result;
            }
        }

        public bool CanStore
        {
            get
            {
                var result = !string.IsNullOrEmpty(Name) && !string.IsNullOrEmpty(Date) && !string.IsNullOrEmpty(Month) && !string.IsNullOrEmpty(Year) &&
                       !string.IsNullOrEmpty(Email) && !string.IsNullOrEmpty(Height) && !string.IsNullOrEmpty(Pin);
                return result;
            }
        }

        public bool StoreRunning
        {
            get { return !_storeRunning; }
            set
            {
                if (value.Equals(_storeRunning)) return;
                _storeRunning = value;
                NotifyOfPropertyChange(() => StoreRunning);
            }
        }

        public async void Store()
        {
            StoreRunning = true;

            //Sends message to stop camera.
            ViewModelEventAggregator.EventAggregator.Publish(new InitiateCameraMessage() { Start = false });

         
            var newUser = string.IsNullOrEmpty(UserToEdit.Name);

            //TODO not sure this is required anymore
            var photoPath = Utils.ImagePath + UserToEdit.PhotoLink;
            bool newPhoto = PhotoManager.UpdateDefaultFileToProfileName(photoPath);

            //TODO Look into refacturing this, maybe add a bool to state if its a new user or not. alot of the code can be removed 
            BaseMessage msg;
            if (newUser)
            {
                //   UserToEdit = new User();
                UpdateUserProperties(newPhoto);

                //var photoPath = Utils.ImagePath + UserToEdit.PhotoLink;
                //newPhoto = PhotoManager.UpdateDefaultFileToProfileName(photoPath);

                msg = new NewUserMessage
                    {
                        Sender = GetType(),
                        NewUser = UserToEdit,
                        PhotoRoot = Utils.ImagePath
                    };
            }
            else
            {
                UpdateUserProperties(newPhoto);
                // var photoPath = Utils.ImagePath + UserToEdit.PhotoLink;
                // newPhoto = PhotoManager.UpdateDefaultFileToProfileName(photoPath);

                msg = new UserToEditCompleteMessage
                    {
                        Sender = GetType(),
                        User = UserToEdit,
                        PhotoRoot = Utils.ImagePath,
                        NewPhoto = newPhoto
                    };
            }

            bool newFingerTemplate = false;
            bool newFacialTemplate = false;

            foreach (var biometric in Biometrics.Where(biometric => biometric.Template != null))
            {
                biometric.StoreTemplate(UserToEdit);
                if (biometric.DeviceType == BiometricDeviceType.Facial)
                    newFacialTemplate = true;
                else
                    newFingerTemplate = true;
            }
            //Call dispose in order to free up camera and other stuff. This is important.
            ProfileImage = null;

            //Task.Run(() => _datasService.UpdateUser(UserToEdit, newPhoto, newFacialTemplate, newFingerTemplate));

            // Task.Run(() =>
            //   {

            bool uploadComplete = false;


            await Task.Run(() =>
                {
                    try
                    {
                        var datasService = new DataService();
                        datasService.UpdateUser(UserToEdit, newPhoto, newFacialTemplate, newFingerTemplate);
                        datasService.Close();
                        uploadComplete = true;
                        //_datasService.UpdateUser(UserToEdit, newPhoto, newFacialTemplate, newFingerTemplate);
                    }
                    catch (Exception e)
                    {
                        uploadComplete = false;

                        ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage()
                        {
                            Sender = GetType(),
                            Message = "Upload failed: " + e.Message
                        });

                    }
                });

            
            //try
            //{
            //    var datasService = new DataService();
            //    datasService.UpdateUser(UserToEdit, newPhoto, newFacialTemplate, newFingerTemplate);
            //    datasService.Close();
            //    uploadComplete = true;
            //    //_datasService.UpdateUser(UserToEdit, newPhoto, newFacialTemplate, newFingerTemplate);
            //}
            //catch (Exception e)
            //{
            //    uploadComplete = false;

            //    ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage()
            //    {
            //        Sender = GetType(),
            //        Message = "Upload failed: " + e.Message
            //    });

            //}

            StoreRunning = false;

            if (uploadComplete)
            {
                PhotoManager.CleanUpTempFiles();

                Dispose();

                ViewModelEventAggregator.EventAggregator.Publish(msg);
            }
            else
                //Sends message to stop camera.
                ViewModelEventAggregator.EventAggregator.Publish(new InitiateCameraMessage() { Start = true });

        }

        public void TakeProfilePicture()
        {
            _takeProfilePicture = true;
        }

        public void CancelEdit()
        {

            //Sends message to stop camera.
            ViewModelEventAggregator.EventAggregator.Publish(new InitiateCameraMessage() { Start = false });
            //Call dispose in order to free up camera and other stuff. This is important.
            PhotoManager.CleanUpTempFiles();

            foreach (var biometric in Biometrics.Where(biometric => biometric.Template != null))
            {
                biometric.RemoveTemplate(UserToEdit);
            }

            ProfileImage = null;

            Dispose();

            ViewModelEventAggregator.EventAggregator.Publish(new CancelUserEditMessage { Sender = GetType() });
        }

        private void UpdateUserProperties(bool newPhoto)
        {
            UserToEdit.Name = Name;
            UserToEdit.Height = Height;
            UserToEdit.UserLevel = UserLevel;
            UserToEdit.Email = Email;
            UserToEdit.Pin = Pin;
            UserToEdit.MobileNumber = MobileNumber;
            UserToEdit.DateOfBirth = UpdateUserBirthdate();

            if (newPhoto)
            {
                UserToEdit.ProfilePhoto = ProfileImage.Clone();
                UserToEdit.HasProfilePhoto = true;
            }

            //todo Check for finger and facial profiles.
        }

        public void KeyPress(object sender, KeyEventArgs e)
        {

            if (e.Key != Key.Tab && !System.Text.RegularExpressions.Regex.IsMatch(e.Key.ToString(), @"\d+"))
                e.Handled = true;
        }

        private string UpdateUserBirthdate()
        {
            return Date + "/" + Month + "/" + Year;
        }

        public void HandleMessage(NewWebcamImageMessage msg)
        {
            Image img = msg.Image;

            using (MemoryStream memory = new MemoryStream())
            {
                img.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                bitmapImage.Freeze();

                WebCamBitmapImage = bitmapImage;

                if (_takeProfilePicture)
                    lock (_pictureLock)
                    {
                        if (_takeProfilePicture)
                        {
                            UpdateAndStoreProfileImage(bitmapImage, img);
                        }
                    }
            }

        }

        private void UpdateAndStoreProfileImage(BitmapImage bitmapImage, Image img)
        {
            ProfileImage = bitmapImage;
            PhotoManager.StorePhoto(img);
            _takeProfilePicture = false;
        }

        //public void HandleMessage(EnrollResponsMessage msg)
        //{
        //    Status = "Succes";
        //}

        //public void HandleMessage(EnrollErrorResponsMessage msg)
        //{
        //    Status = msg.Error;
        //}

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
        }

        public void HandleMessage(BaseMessage message)
        {
            //this function is empty by design.
        }

        public void ChangedUserToEdit(User userToEdit)
        {
            UserToEdit = userToEdit;
            UpdateAllPropertiesFromUser();

            StoreButtonStateText = "Update";
        }
    }
}
