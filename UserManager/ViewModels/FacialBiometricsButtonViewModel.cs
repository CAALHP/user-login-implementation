﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Events.Types;
using Caliburn.Micro;
using UserManager.EventAggregators;
using UserManager.EventMessages;
using UserManager.Model;
using UserManager.Properties;
using Action = System.Action;

namespace UserManager.ViewModels
{
    public class FacialBiometricsButtonViewModel : BiometricsButtonViewModel, IHandle<BaseMessage>
    {
        private WebcamManager _webcamManager;

        public FacialBiometricsButtonViewModel(User u)
            : base(u)
        {
            ViewModelEventAggregator.EventAggregator.Subscribe(this);

            DeviceType = BiometricDeviceType.Facial;

            TypeName = "Ansigt";

            UserBiometricsExsists = u.FacialTemplateExsists;

            UpdateLayout();

            try
            {
                _webcamManager = new WebcamManager();
                _webcamManager.StartCapture();
            }
            catch (Exception)
            {
                _webcamManager = null;
            }

        }

        private void UpdateLayout()
        {
            if (UserBiometricsExsists)
            {
                Icon = @"\Images\OkIcon.png";
                DeviceStatus = "";
            }
            else
            {
                Icon = @"\Images\ErrorIcon.png";
                DeviceStatus = "Tryk her for at oprette";
            }

            IsDeviceEnabled = Utils.FacialDriverEnabled;
        }

        public override void Dispose()
        {
            if (_webcamManager != null)
                _webcamManager.StopCapture();
        }

        public override void StoreTemplate(User userToEdit)
        {
            userToEdit.FacialTemplate = Template;
            userToEdit.FacialTemplateExsists = true;
        }

        public override void RemoveTemplate(User userToEdit)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new RequestFacialRemoveMessage() { UserId = User.Id, Sender = GetType() });
        }

        public override void Enroll()
        {
            EnrollingRunning = true;
            if (_webcamManager != null)
                _webcamManager.StopCapture();

            Thread.Sleep(1000);
            ViewModelEventAggregator.EventAggregator.Publish(new RequestFacialEnrollMessage() { UserId = User.Id, Sender = GetType() });

        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
        }

        public void HandleMessage(BaseMessage message)
        {
            //this function is empty by design.
        }

        public void HandleMessage(EnrollResponsMessage msg)
        {
            if (string.Equals(msg.UserId, User.Id))
            {
                Template = msg.Template;
                UserBiometricsExsists = true;
            }
            if (_webcamManager != null)
                _webcamManager.StartCapture();

            UpdateLayout();

            EnrollingRunning = false;

            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage()
            {
                Sender = GetType(),
                Message = "Enroll Facialprofile complete"
            });
        }

        public void HandleMessage(EnrollErrorResponsMessage msg)
        {
            if (_webcamManager != null)
                _webcamManager.StartCapture();

            EnrollingRunning = false;

            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage()
            {
                Sender = GetType(),
                Message = "Enroll Facialprofile failed" + msg.Error
            });
        }

        public void HandleMessage(UpdateBiometricsMessage msg)
        {
            IsDeviceEnabled = Utils.FacialDriverEnabled;
        }



        public void HandleMessage(InitiateCameraMessage msg)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    if (_webcamManager == null) return;
                    if (msg.Start)
                        _webcamManager.StartCapture();
                    else
                        _webcamManager.StopCapture();
                }));
        }
    }
}
