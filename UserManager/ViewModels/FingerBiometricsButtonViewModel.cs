﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;
using Caliburn.Micro;
using UserManager.EventAggregators;
using UserManager.EventMessages;
using UserManager.Model;

namespace UserManager.ViewModels
{
    public class FingerBiometricsButtonViewModel : BiometricsButtonViewModel, IHandle<BaseMessage>
    {
        
        public FingerBiometricsButtonViewModel(User u)
            : base(u)
        {
            ViewModelEventAggregator.EventAggregator.Subscribe(this);

            DeviceType = BiometricDeviceType.Finger;

            TypeName = "Finger";

            UserBiometricsExsists = u.FingerTemplateExsists;

            UpdateLayout();

        }

        private void UpdateLayout()
        {
            if (UserBiometricsExsists)
            {
                Icon = @"\Images\OkIcon.png";
                DeviceStatus = "";
            }
            else
            {
                Icon = @"\Images\ErrorIcon.png";
                DeviceStatus = "Tryk her for at oprette";
            }

            IsDeviceEnabled = Utils.FingerDriverEnabled;
        }

        //public byte[] FingerTemplate { get; private set; }

        public override void Enroll()
        {
            EnrollingRunning = true;
            
            ViewModelEventAggregator.EventAggregator.Publish(new RequestFingerEnrollMessage() { UserId = User.Id, Sender = GetType() });
        }

        public override void Dispose()
        {
            //clean up here if something needs to be.
        }

        public override void StoreTemplate(User userToEdit)
        {
            userToEdit.FingerTemplate = Template;
            userToEdit.FingerTemplateExsists = true;
        }

        public override void RemoveTemplate(User userToEdit)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new RequestFingerRemoveMessage() { UserId = User.Id, Sender = GetType() });
        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
        }

        public void HandleMessage(BaseMessage message)
        {
            //this function is empty by design.
        }


        public void HandleMessage(UpdateBiometricsMessage msg)
        {
            IsDeviceEnabled = Utils.FingerDriverEnabled;
        }

        public void HandleMessage(FingerEnrollResponsMessage msg)
        {
            if (string.Equals(msg.UserId, User.Id))
            {
                Template = msg.Template;
                UserBiometricsExsists = true;
            }

            EnrollingRunning = false;

            UpdateLayout();

            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage()
            {
                Sender = GetType(),
                Message = "Enroll fingerprint complete"
            });

        }

        public void HandleMessage(FingerEnrollErrorMessage msg)
        {
            EnrollingRunning = false;

            UpdateLayout();

            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage()
            {
                Sender = GetType(),
                Message = "Enroll fingerprint failed: "
            });
        }
    }
}
