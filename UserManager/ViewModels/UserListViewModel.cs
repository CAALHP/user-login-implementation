﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CAALHP.Events.Types;
using Caliburn.Micro;
using UserManager.EventAggregators;
using UserManager.EventMessages;

using UserManager.Model;
using Action = System.Action;

namespace UserManager.ViewModels
{
    [Export(typeof(UserListViewModel))]
    public class UserListViewModel : BaseViewModel, IHandle<BaseMessage>
    {
        private ObservableCollection<User> _userList = new ObservableCollection<User>();
        private User _selectedUser;
        private UserProfile _loggedUser;

        public UserListViewModel()
        {
            ViewModelEventAggregator.EventAggregator.Subscribe(this);


        }

        public ObservableCollection<User> UserList
        {
            get
            {
                return _userList;
            }
            set
            {
                if (Equals(value, _userList)) return;
                _userList = value;
                NotifyOfPropertyChange(() => UserList);
                // NotifyOfPropertyChange(() => BoundUserList);
            }
        }

        //public ObservableCollection<User> BoundUserList
        //{
        //    get
        //    {
        //        //if (_loggedUser == null)
        //        //    return null;
        //        //if (_loggedUser.UserLevel == CareStoreUserLevel.Caretaker)
        //        //    return _userList;
        //        ////if (_loggedUser.UserLevel == CareStoreUserLevel.Citizen)
        //        ////    return new ObservableCollection<User>(_userList.Where(x => x.Id == _loggedUser.UserId).ToList());

        //        //return null;

        //        try
        //        {
        //            if (_loggedUser.UserLevel == CareStoreUserLevel.Citizen)
        //                return new ObservableCollection<User>(_userList.Where(x => x.Id == _loggedUser.UserId).ToList());
        //        }
        //        catch (Exception)
        //        {

        //            return _userList;
        //        }


        //    }
        //}

        public User SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                if (Equals(value, _selectedUser)) return;
                _selectedUser = value;
                NotifyOfPropertyChange(() => SelectedUser);
                if (value != null)
                    ViewModelEventAggregator.EventAggregator.Publish(new UserToEditMessage { UserToEdit = _selectedUser, Sender = GetType() });
            }
        }

        public void HandleMessage(UserListResponseMessage list)
        {
            Application.Current.Dispatcher.BeginInvoke((Action)(() =>
            {

                if (UserList != null)
                    UserList.Clear();

                var ulist = list.Users;

                //var updatedUserIdList = (from a in UserList select a.Id).ToList();

                //ulist.RemoveAll(x => updatedUserIdList.Contains(x.UserId));

                var sourcePath = list.PhotoRoot;

                foreach (var entity in ulist)
                {
                    try
                    {
                        //if(entity.HasPhoto)
                        Utils.GetUserImage(entity.PhotoLink, sourcePath);

                        var user = new User(entity.UserId, entity.CreationTime)
                        {
                            Name = entity.FullName,
                            DateOfBirth = entity.DateOfBirth,
                            Height = entity.Height,
                            UserLevel = entity.UserLevel,
                            Email = entity.Email,
                            MobileNumber = entity.MobileNumber,
                            FacialTemplateExsists = entity.FacialProfile,
                            FingerTemplateExsists = entity.FingerPrint,
                            Pin = entity.Pin,
                            HasProfilePhoto = entity.HasPhoto
                        };

                        UserList.Add(user);

                    }
                    catch (Exception e)
                    {
                        //TODO make something with a status message here.
                        throw new Exception(e.Message);
                    }
                }
            }));

            NotifyOfPropertyChange(() => UserList);
            NotifyOfPropertyChange(string.Empty);

        }

        public void HandleMessage(NewUserMessage message)
        {
            var newUser = message.NewUser;
            UserList.Add(newUser);
            SelectedUser = null;
        }

        private void HandleMessage(UserToEditCompleteMessage o)
        {
            var editedUser = o.User;

            var oldUser = UserList.FirstOrDefault(x => x.Id == editedUser.Id);
            if (oldUser != null)
            {
                int index = UserList.IndexOf(oldUser);
                UserList[index] = editedUser;
            }
            else
            {
                UserList.Add(editedUser);
            }

            SelectedUser = null;
        }

        private void HandleMessage(CancelUserEditMessage o)
        {
            SelectedUser = null;
        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
        }

        private void HandleMessage(UserLoggedInMessage o)
        {
            _loggedUser = o.User;

            NotifyOfPropertyChange(() => UserList);
            NotifyOfPropertyChange(string.Empty);

        }

        private void HandleMessage(UserLoggedOutMessage o)
        {
            _loggedUser = null;

            NotifyOfPropertyChange(() => UserList);
            NotifyOfPropertyChange(string.Empty);
        }

        public void HandleMessage(BaseMessage message)
        {
            //this function is empty by design.
        }
    }
}
