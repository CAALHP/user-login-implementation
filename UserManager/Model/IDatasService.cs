﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManager.Model
{
    public interface IDatasService
    {
        void UpdateUser(User profile, bool newPicture, bool newFaceTemplate, bool newFingerTemplate);
        void UpdateFacialTemplate(string id, byte[] template);
        void UpdateFingerTemplate(string id, byte[] template);
        void UpdateProfilePicture(string id, byte[] picture);
        //List<UserProfileDataObject> GetUsers();
        //byte[] GetProfilePicture(string id);
        //byte[] GetFacialTemplate(string id);
        //byte[] GetFingerTemplate(string id);
        void RemoveUser(string id);
    }
}
