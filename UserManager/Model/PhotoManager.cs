﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManager.Model
{
    public static class PhotoManager
    {
        private static readonly string _defaultPhotoLink = Utils.ImagePath + "temp.jpg";

        public static bool StorePhoto(Image img, string photoLink = null)
        {
            try
            {
                img.Save(string.IsNullOrEmpty(photoLink) ? DefaultPhotoLink : photoLink, ImageFormat.Jpeg);
            }
            catch (Exception e)
            {

                return false;
            }

            return true;
        }

        public static string DefaultPhotoLink { get { return _defaultPhotoLink; } }

        public static void CleanUpTempFiles()
        {
            if(File.Exists(DefaultPhotoLink))
                File.Delete(DefaultPhotoLink);
        }

        public static bool UpdateDefaultFileToProfileName(string photoLink)
        {

            if (File.Exists(DefaultPhotoLink))
            {
                if (File.Exists(photoLink))
                    File.Delete(photoLink);
                File.Move(DefaultPhotoLink, photoLink);

                return true;
            }

            return false;
        }


    }
}
