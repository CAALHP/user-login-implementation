﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using TouchlessLib;
using System.Windows;
using Touchless.Vision.Camera;
using Touchless.Vision.Contracts;
using UserManager.EventAggregators;
using UserManager.EventMessages;

namespace UserManager.Model
{
    public class WebcamManager
    {



        //private Camera cam;
        private bool _camEnabled;

        // public TouchlessMgr _touchlessMgr { get; set; }
        private CameraFrameSource _frameSource;

        public WebcamManager()
        {
            _camEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("enablewebcam"));
            //if (_camEnabled)
            //    _touchlessMgr = new TouchlessMgr();
        }

        public void StartCapture()
        {
            if (!_camEnabled) return;

            DoSendImageInThread();

            //new Thread(DoSendImageInThread).Start();

        }

        public void StopCapture()
        {
            //if (!_camEnabled) return;

            //if (cam != null)
            //    cam.OnImageCaptured -= cam_OnImageCaptured;
            ////cam.Dispose();
            ////_touchlessMgr.CurrentCamera.Dispose();
            //cam = null;
            //if (_touchlessMgr != null && _touchlessMgr.CurrentCamera != null)
            //{
            //    //_touchlessMgr.CurrentCamera = null;
            //    _touchlessMgr.Dispose();
            //}

            //_touchlessMgr = null;


            ThrashOldCamera();
        }

        private void DoSendImageInThread()
        {

            try
            {
                Camera camera = CameraService.AvailableCameras.FirstOrDefault(cam2 => cam2.ToString().StartsWith("Logi")) ?? CameraService.AvailableCameras.FirstOrDefault();
                //Camera c = cam;

                SetFrameSource(new CameraFrameSource(camera));
                _frameSource.Camera.CaptureWidth = 600;
                _frameSource.Camera.CaptureHeight = 600;
                _frameSource.Camera.Fps = 20;
                _frameSource.NewFrame += OnImageCaptured;

                // pictureBoxDisplay.Paint += new PaintEventHandler(drawLatestImage);
                _frameSource.StartFrameCapture();
                


            }
            catch (Exception ex)
            {
                //comboBoxCameras.Text = "Select A Camera";
                ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage()
                {
                    Sender = GetType(),
                    Message = "Failed to initialize camera"
                });
            }


            //if (_touchlessMgr == null)
            //    _touchlessMgr = new TouchlessMgr();

            //for (int i = 0; i < 5; i++)
            //{
            //    try
            //    {
            //        if (cam == null)
            //        {

            //            foreach (var cam2 in _touchlessMgr.Cameras.Where(cam2 => cam2.ToString().StartsWith("Logi")))
            //            {
            //                cam2.CaptureHeight = 600;
            //                cam2.CaptureWidth = 600;
            //                _touchlessMgr.CurrentCamera = cam2;

            //                cam = cam2;
            //                Thread.Sleep(1000);
            //            }
            //            if (cam == null)
            //            {
            //                var tmpcam = _touchlessMgr.Cameras.FirstOrDefault();
            //                if (tmpcam != null)
            //                {
            //                    tmpcam.CaptureHeight = 600;
            //                    tmpcam.CaptureWidth = 600;
            //                    _touchlessMgr.CurrentCamera = tmpcam;

            //                    cam = tmpcam;
            //                }
            //            }
            //        }
            //        if (cam != null)
            //            break;
            //    }
            //    catch (Exception)
            //    {

            //        cam = null;

            //    }
            //    Thread.Sleep(1000);
            //}



            //if (cam != null)
            //    cam.OnImageCaptured += cam_OnImageCaptured; //Do NOT subscribe for continuous

            //_imageBytes = imageToByteArray(cam.GetCurrentImage());

            //if (_imageBytes != null)
            //{
            //    //Upload image to the server (insulin monitor) at behandlerskærm
            //    _proxy.UploadImage(_imageBytes);
            //    Thread.Sleep(3000);
            //}

        }

        private void ThrashOldCamera()
        {
            // Trash the old camera
            if (_frameSource != null)
            {
                _frameSource.NewFrame -= OnImageCaptured;
                Thread.Sleep(100);
                _frameSource.StopFrameCapture();
                Thread.Sleep(200);

                _frameSource.Camera.Dispose();
                SetFrameSource(null);
                //pictureBoxDisplay.Paint -= new PaintEventHandler(drawLatestImage);
            }
        }

        private void SetFrameSource(CameraFrameSource cameraFrameSource)
        {
            if (_frameSource == cameraFrameSource) return;
            _frameSource = cameraFrameSource;
        }

        private void OnImageCaptured(IFrameSource frameSource, Frame frame, double fps)
        {
            System.Drawing.Image im = frame.Image;

            ViewModelEventAggregator.EventAggregator.Publish(new NewWebcamImageMessage { Image = im, Sender = GetType() });
        }

        //private void cam_OnImageCaptured(object sender, CameraEventArgs e)
        //{
        //    //throw new NotImplementedException();

        //    //cam.OnImageCaptured -= cam_OnImageCaptured; //Do NOT subscribe for continuous

        //    System.Drawing.Image im = e.Image;

        //    ViewModelEventAggregator.EventAggregator.Publish(new NewWebcamImageMessage { Image = im, Sender = GetType() });

        //}


        //public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
        //    return ms.ToArray();

        //}
    }
}
