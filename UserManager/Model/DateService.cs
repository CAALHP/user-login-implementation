﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CareStoreServiceContracts;
using UserManager.EventAggregators;
using UserManager.EventMessages;

namespace UserManager.Model
{
    public class DataService : IDatasService
    {
        private ICareStoreMarketplaceContract _market;
        private ChannelFactory<ICareStoreMarketplaceContract> channel;

        public DataService()
        {
            try
            {
                //binding
                var binding = new BasicHttpBinding();
                //var binding = new BasicHttpsBinding();
                //endpoint
                binding.MaxBufferSize = 2138483646;
                binding.MaxReceivedMessageSize = 2138483646;
                binding.SendTimeout = new System.TimeSpan(1,0,0,0);
                binding.ReceiveTimeout = new System.TimeSpan(1,0,0,0);
               
                var endpoint = new EndpointAddress(ConfigurationManager.AppSettings.Get("marketplaceAddress"));
                //channelfactory
               // var channelFactory = new ChannelFactory<ICareStoreMarketplaceContract>(binding, endpoint);

                channel = new ChannelFactory<ICareStoreMarketplaceContract>(binding, endpoint);
                //System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                //    (sender, certificate, chain, sslPolicyErrors) => true;
                //channelFactory.e
                //_market = channelFactory.CreateChannel();
                _market = channel.CreateChannel();
                //channelFactory.State
            }
            catch (Exception e)
            {
                
                throw;
            }
            

        }

        public void Close()
        {
            channel.Close();
        }


        public void UpdateUser(User profile, bool newPicture, bool newFaceTemplate, bool newFingerTemplate)
        {
            var userDto = new UserProfileDataObject()
                {
                    Id = profile.Id,
                    CreationTime = profile.CreationTime,
                    Name = profile.Name,
                    DateOfBirth = profile.DateOfBirth,
                    Height = profile.Height,
                    Email = profile.Email,
                    MobileNumber = profile.MobileNumber,
                    Pin = profile.Pin,
                    UserLevel = profile.UserLevel.ToString(),
                    HasProfileImage = profile.HasProfilePhoto,
                    HasFacialProfileTemplate = profile.FacialTemplateExsists,
                    HasFingerPrintTemplate = profile.FingerTemplateExsists
                    
                };


            byte[] facialTemplate = newFaceTemplate ? profile.FacialTemplate : null;
            byte[] fingerTemplate = newFingerTemplate ? profile.FingerTemplate : null;
            byte[] profilePicture;
            try
            {
                var photoPath = Utils.ImagePath + profile.PhotoLink;
                profilePicture = newPicture ? File.ReadAllBytes(photoPath) : null;  
            }
            catch (Exception)
            {
                userDto.HasProfileImage = false;
                profilePicture = null;
            }

            if(userDto == null)
                throw new ArgumentNullException();

            try
            {
                _market.UpdateUser(userDto, profilePicture, facialTemplate, fingerTemplate);
            }
            catch (Exception e)
            {
                throw e;
                //MessageBox.Show();
                //ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage()
                //{
                //    Sender = GetType(),
                //    Message = "Upload failed: " + e.InnerException.Message
                //});
            }
            
        }

        //public void UpdateUser(User profile, bool newPicture, bool newFaceTemplate, bool newFingerTemplate)
        //{
        //    throw new NotImplementedException();
        //}

        public void UpdateFacialTemplate(string id, byte[] template)
        {
            _market.UpdateFacialTemplate(id, template);
        }

        public void UpdateFingerTemplate(string id, byte[] template)
        {
            _market.UpdateFingerTemplate(id,template);
        }

        public void UpdateProfilePicture(string id, byte[] picture)
        {
            _market.UpdateProfilePicture(id, picture);
        }

        public void RemoveUser(string id)
        {
            _market.RemoveUser(id);
        }
    }
}
