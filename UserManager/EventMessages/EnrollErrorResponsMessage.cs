﻿

namespace UserManager.EventMessages
{
    public class EnrollErrorResponsMessage: BaseMessage
    {
        public string Error { get; set; }
    }
}
