﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManager.Model;

namespace UserManager.EventMessages
{
    public class NewUserMessage: BaseMessage
    {
        public User NewUser { get; set; }
        public string PhotoRoot { get; set; }
    }
}
