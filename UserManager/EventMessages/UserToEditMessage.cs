﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManager.Model;

namespace UserManager.EventMessages
{
    public class UserToEditMessage:BaseMessage 
    {
        public User UserToEdit { get; set; }
    }
}
