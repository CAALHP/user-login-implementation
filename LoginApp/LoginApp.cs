﻿using System.AddIn;
using Plugins.AppPluginAdapters;

namespace Plugins.LoginApp
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Login App", Version = "1.0.0.0")]
    public class LoginApp : AppPluginAdapter
    {
        public LoginApp()
        {
            App = new LoginAppImplementation();
        }
    }
}
