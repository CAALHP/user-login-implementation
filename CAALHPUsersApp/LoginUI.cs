﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using CAALHPUser.Properties;
using GalaSoft.MvvmLight.Messaging;
using CAALHP.Events;
using CAALHP.Events.Types;
using GalaSoft.MvvmLight.Threading;


namespace CAALHPUser
{


    public partial class LoginUI : Form
    {

        #region fields

        //private IUser _userHandler = new User();
        private string _pincode;
        private Dictionary<int, UserProfile> _userDictionary;
        private UserProfile _currentUser;
        private string _imgPath;

        #endregion

        #region constructor

        public LoginUI()
        {
            InitializeComponent();

            HideStepTwoPanels();

            LoginButton.Enabled = false;

            StatusLabel.Text = "";
            ErrorMsgLabel.Text = "";
            _pincode = "";
            this.PinCodeTextBox.Text = _pincode;
            //#if DEBUG
              //  Debugger.Launch();
            //#endif

            Messenger.Default.Register<GenericMessage<UserListResponseEvent>>(this, HandleUserListResponse);
            Messenger.Default.Register<GenericMessage<AuthenticationResponseEvent>>(this, HandleAuthencationResponse);
            Messenger.Default.Register<GenericMessage<UserLoggedOutEvent>>(this, HandleUserLoggedOutEvent);
            Messenger.Default.Register<GenericMessage<UserLoggedInEvent>>(this, HandleUserLoggedInEvent);
            Messenger.Default.Register<NotificationMessage>(this, HandleShowNotification);
            //load users on form load



            _imgPath = AssemblyDirectory + "\\Images\\";
            if (!Directory.Exists(_imgPath))
            {
                Directory.CreateDirectory(_imgPath);
            }

            RequestLoadUsers();
            
        }

        #endregion

        #region private methods

        private void IsLogged(bool x)
        {
            //ThumbnailPanel.Enabled = !x;
            ThumbnailPanel.Enabled = true;
            if (!x) return;

            StatusLabel.Text = "";

            foreach (Panel thumbnailButton in ThumbnailPanel.Controls)
            {
                foreach (var userIndex in thumbnailButton.Controls.OfType<UserButton>().Select(userBtn => userBtn.UserIndex))
                {
                    thumbnailButton.Enabled = _currentUser.UserId != _userDictionary[userIndex].UserId;
                    break;
                }
            }

            var statusmsg = "Godkendt";
            //CreateLoginEvent();

            BeginInvoke((MethodInvoker)delegate
                {
                    StatusLabel.Text = statusmsg;
                    Hide();
                });
        }

        private void HideStepTwoPanels()
        {
            StatusPanel3.Visible = false;
            NewUserPanel.Visible = false;
            panelKeypad.Visible = false;
        }

        private List<Image> GetImages(List<UserProfile> users)
        {
            List<Image> images = new List<Image>();
            foreach (var user in users)
            {
                try
                {
                    var image = Image.FromFile(user.PhotoLink);
                    images.Add(image);
                }
                catch (Exception)
                {
                    //skip
                }
            }
            return images;
        }

        private Image GetUserImage(string photoLink, string sourcePath)
        {
            Image image = null;

            if (string.IsNullOrEmpty(photoLink))
                return Resources.Default;

            try
            {
                var targetImgPath = Path.Combine(_imgPath, photoLink);
                var sourceImgPath = Path.Combine(sourcePath, photoLink);

                CopyImagefromSource(sourceImgPath, targetImgPath);
                image = Image.FromFile(targetImgPath);
            }
            catch (Exception)
            {
                image = Resources.Default;
            }

            return image;
        }

        private void CopyImagefromSource(string sourceImgPath, string targetImgPath)
        {
            if (!File.Exists(targetImgPath) ||
                File.GetLastWriteTime(targetImgPath) < File.GetLastWriteTime(sourceImgPath))
            {
                File.Copy(sourceImgPath, targetImgPath, true);
            }

        }

        static private string AssemblyDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        #endregion

        #region Non UI EventHandlers

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            this.Activate();
        }


        private void HandleShowNotification(NotificationMessage obj)
        {
            if (Visible)
                this.Hide();
            this.Show();
            this.Activate();
        }

        //TODO this function should do some logout magic
        private void HandleUserLoggedOutEvent(GenericMessage<UserLoggedOutEvent> obj)
        {
            foreach (Panel thumbnailButton in ThumbnailPanel.Controls)
            {
                thumbnailButton.Enabled = true;
            }

            _currentUser = null;
            IsLogged(false);
        }

        private void HandleUserLoggedInEvent(GenericMessage<UserLoggedInEvent> obj)
        {

            this.BeginInvoke((MethodInvoker)(() =>
            {
                panelKeypad.Hide();
                panelKeypad.Enabled = true;
            }));
            IsLogged(true);
            
        }

        private void HandleUserListResponse(GenericMessage<UserListResponseEvent> obj)
        {

            ThumbnailPanel.BeginInvoke((MethodInvoker)delegate
                {
                    //this.Show();
                    var users = obj.Content.Users;
                    var photoRoot = obj.Content.PhotoRoot;

                    ThumbnailPanel.Controls.Clear();
                    //stops the layout from updating
                    this.SuspendLayout();

                    //var images = GetImages(users);
                    _userDictionary = new Dictionary<int, UserProfile>();

                    int panelmargin = 10;

                    for (var i = 0; i < users.Count; i++)
                    {
                        if (string.IsNullOrEmpty(users[i].UserId)) continue;

                        var userPanel = new Panel();
                        var userButton = new UserButton(i);
                        var userNameLabel = new Label();

                        //define layout and margins for each userthumbnail
                        // ThumbnailPanel.AutoSize = true;
                        userPanel.AutoSize = true;

                        userButton.Width = 93;
                        userButton.Height = 121;
                        userButton.TabStop = false;
                        userButton.FlatStyle = FlatStyle.Flat;
                        userButton.FlatAppearance.BorderSize = 0;
                        userButton.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);

                        userPanel.Width = 100;
                        userPanel.Left = panelmargin;
                        userNameLabel.Top = userButton.Height + 5;

                        //subscribe to event when image is clicked.
                        userButton.Click += new EventHandler(UserButtonClickEvent);


                        //add images and text to userPanel
                        userButton.Image = GetUserImage(users[i].PhotoLink, photoRoot);
                        userNameLabel.Text = users[i].FullName;

                        //add controls to parent controls
                        userPanel.Controls.Add(userButton);
                        userPanel.Controls.Add(userNameLabel);


                        ThumbnailPanel.Controls.Add(userPanel);

                        //add event to dynamically created buttons

                        _userDictionary.Add(i, users[i]);
                    }

                    //resumes update of layout
                    this.ResumeLayout();
                    ThumbnailPanel.Invalidate();
                });

        }

        private void HandleAuthencationResponse(GenericMessage<AuthenticationResponseEvent> obj)
        {

            var result = obj.Content;
            var type = result.ReqType;
            var isLogged = false;

            if (type == RequestTypes.AuthByFinger)
            {
                if (result.Response)
                {
                    isLogged = true;
                }
                else
                {
                    if (result.Message.ToLower() == "no user")
                    {
                        this.BeginInvoke((MethodInvoker)(() => NewUserPanel.Show()));

                    }
                    else if (result.Message.ToLower() == "noscanner")
                    {
                        this.BeginInvoke((MethodInvoker)(() => panelKeypad.Show()));
                    }
                    else
                    {
                        var statusmsg = "Genkendelse mislykkedes";

                        this.BeginInvoke((MethodInvoker)delegate
                            {
                                StatusLabel.Text = statusmsg;
                                ErrorMsgLabel.Text = result.Message;
                            });
                    }
                }
            }
            else    if (type == RequestTypes.AuthByPincode)
            {
                if (result.Response)
                {
                    isLogged = true;
                    this.BeginInvoke((MethodInvoker)(() =>
                        {
                            panelKeypad.Hide();
                            panelKeypad.Enabled = true;
                        }));
                }
                else
                {
                    var statusmsg = "Forkert pin";
                    this.BeginInvoke((MethodInvoker)(() =>
                    {
                        StatusLabel.Text = statusmsg;
                        panelKeypad.Enabled = true;
                    }));
                }
            }
            else if (type == RequestTypes.Enroll)
            {
                if (result.Response)
                {

                    var statusmsg = "Oprettelse lykkedes";
                    var statusmsg2 = "Du kan nu logge ind";

                    this.BeginInvoke((MethodInvoker)delegate
                        {
                            StatusLabel.Text = statusmsg;
                            ErrorMsgLabel.Text = statusmsg2;
                            NewUserPanel.Hide();
                        });
                }
                else if (result.Message.ToLower() == "noscanner")
                {
                    this.BeginInvoke((MethodInvoker)(() => panelKeypad.Show()));
                }
                else if (result.Message.ToLower() == "User already exsists")
                {
                    throw new NotImplementedException();
                }
                else
                {

                    var statusmsg = "Oprettelse mislykkedes";

                    this.BeginInvoke((MethodInvoker)delegate
                        {
                            StatusLabel.Text = statusmsg;
                            ErrorMsgLabel.Text = result.Message;
                        });
                }

                EnrollButton.Enabled = true;
            }
            else if (type == RequestTypes.CheckforScanner)
            {
                //TODO some no scanner thing here
                if (!result.Response)
                    this.BeginInvoke((MethodInvoker)(() => panelKeypad.Show()));

                this.BeginInvoke((MethodInvoker)(() => StatusLabel.Text = result.Message));
            }

            IsLogged(isLogged);
        }

        #endregion

        #region Control/UI event handlers

        //button click event
        private void UserButtonClickEvent(object sender, EventArgs e)
        {
            //todo add timer to reenable panel.
            ThumbnailPanel.Enabled = false;

            if (panelKeypad.Visible || StatusPanel3.Visible || NewUserPanel.Visible)
                HideStepTwoPanels();

            if (!string.IsNullOrEmpty(StatusLabel.Text))
            {
                StatusLabel.Text = "";
                ErrorMsgLabel.Text = "";
            }


            var userIndex = (sender as UserButton).UserIndex;
            _currentUser = _userDictionary[userIndex];
            
            RequestAuth();

        }


        private void EnrollButtonClick(object sender, EventArgs e)
        {
            EnrollButton.Enabled = false;
            ThumbnailPanel.Enabled = false;

            RequestEnroll();
        }


        //Input button 0-9
        private void NumericButtonClick(object sender, EventArgs e)
        {
            var snd = sender as Button;

            if (_pincode.Length >= 4) return;

            if (snd != null) _pincode = _pincode + snd.Text;
            PinCodeTextBox.Text = _pincode;

            LoginButton.Enabled = _pincode.Length == 4;

        }

        //deletebutton - clears entire pincode.
        private void ClearPinClick(object sender, EventArgs e)
        {
            _pincode = "";
            PinCodeTextBox.Text = _pincode;
        }

        private void ClearPin()
        {
            _pincode = "";
            PinCodeTextBox.Text = _pincode;
        }

        //C button - clears the last added number.
        private void DeleteLastEntryClick(object sender, EventArgs e)
        {
            if (_pincode.Length > 0)
            {
                _pincode = _pincode.Substring(0, _pincode.Length - 1);
                PinCodeTextBox.Text = _pincode;
            }

        }

        /// LoginButton...
        private void LoginButtonClick(object sender, EventArgs e)
        {
            panelKeypad.Enabled = false;

            RequestAuth();
        }

        private void LoginUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        #endregion

        #region Raise Event methods

        private void RequestAuth()
        {
            _pincode = PinCodeTextBox.Text;
            AuthenticationRequestEvent request;

            if (string.IsNullOrEmpty(_pincode))
                request = new AuthenticationRequestEvent
                    {
                        Pincode = _pincode,
                        Username = _currentUser.UserId,
                        ReqType = RequestTypes.AuthByFinger
                    };
            else
            {
                request = new AuthenticationRequestEvent
                {
                    Pincode = _pincode,
                    Username = _currentUser.UserId,
                    ReqType = RequestTypes.AuthByPincode
                };

            }


            ClearPin();

            CreateRequestEvent(request);
        }

        private void RequestEnroll()
        {

            var request = new AuthenticationRequestEvent
                {
                    Username = _currentUser.UserName,
                    ReqType = RequestTypes.Enroll
                };

            CreateRequestEvent(request);
        }

        private void RequestScanner()
        {

            var request = new AuthenticationRequestEvent
                {
                    ReqType = RequestTypes.CheckforScanner
                };

            CreateRequestEvent(request);
        }


        private static void CreateRequestEvent(AuthenticationRequestEvent request)
        {
            Messenger.Default.Send<GenericMessage<AuthenticationRequestEvent>>(
                new GenericMessage<AuthenticationRequestEvent>(request));
        }

        private void CreateLoginEvent()
        {
            var request = new UserLoggedInEvent()
                {
                    User = _currentUser,
                    PhotoRoot = _imgPath

                };
            Messenger.Default.Send<GenericMessage<UserLoggedInEvent>>(
                new GenericMessage<UserLoggedInEvent>(request));
        }

        //load user list on form load
        private void RequestLoadUsers()
        {

            //user list request and response events
            var request = new UserListRequestEvent();

            Messenger.Default.Send<GenericMessage<UserListRequestEvent>>(new GenericMessage<UserListRequestEvent>(request));

        }

        #endregion

    }
}