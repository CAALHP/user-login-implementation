﻿namespace CAALHPUser
{
    partial class LoginUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        //public IUser _userHandler;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.userImageList = new System.Windows.Forms.ImageList(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.HeaderPanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panelKeypad = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.LoginButton = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.PinCodeTextBox = new System.Windows.Forms.TextBox();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.StatusPanel2 = new System.Windows.Forms.Panel();
            this.msg2part3 = new System.Windows.Forms.Label();
            this.msg2part2 = new System.Windows.Forms.Label();
            this.msg2part1 = new System.Windows.Forms.Label();
            this.StatusPanel3 = new System.Windows.Forms.Panel();
            this.Msg3part2 = new System.Windows.Forms.Label();
            this.Msg3part1 = new System.Windows.Forms.Label();
            this.NewUserPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.EnrollButton = new System.Windows.Forms.Button();
            this.EnrollLabel = new System.Windows.Forms.Label();
            this.authReLable = new System.Windows.Forms.Label();
            this.ThumbnailPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.Msg1part1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ErrorMsgLabel = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.HeaderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.MainPanel.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panelKeypad.SuspendLayout();
            this.StatusPanel2.SuspendLayout();
            this.StatusPanel3.SuspendLayout();
            this.NewUserPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // userImageList
            // 
            this.userImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.userImageList.ImageSize = new System.Drawing.Size(16, 16);
            this.userImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.HeaderPanel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.MainPanel, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.ThumbnailPanel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Msg1part1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 3, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.34F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.66F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1600, 900);
            this.tableLayoutPanel1.TabIndex = 14;
            // 
            // HeaderPanel
            // 
            this.HeaderPanel.AutoSize = true;
            this.HeaderPanel.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.HeaderPanel, 4);
            this.HeaderPanel.Controls.Add(this.pictureBox1);
            this.HeaderPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.HeaderPanel.Margin = new System.Windows.Forms.Padding(0);
            this.HeaderPanel.Name = "HeaderPanel";
            this.HeaderPanel.Size = new System.Drawing.Size(1600, 138);
            this.HeaderPanel.TabIndex = 16;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CAALHPUser.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(81, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(324, 93);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // MainPanel
            // 
            this.MainPanel.BackColor = System.Drawing.Color.Transparent;
            this.MainPanel.Controls.Add(this.tableLayoutPanel2);
            this.MainPanel.Controls.Add(this.authReLable);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(960, 138);
            this.MainPanel.Margin = new System.Windows.Forms.Padding(0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(320, 762);
            this.MainPanel.TabIndex = 15;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.panelKeypad, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.StatusPanel2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.StatusPanel3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.NewUserPanel, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(320, 762);
            this.tableLayoutPanel2.TabIndex = 22;
            // 
            // panelKeypad
            // 
            this.panelKeypad.AutoSize = true;
            this.panelKeypad.Controls.Add(this.label1);
            this.panelKeypad.Controls.Add(this.LoginButton);
            this.panelKeypad.Controls.Add(this.button12);
            this.panelKeypad.Controls.Add(this.button13);
            this.panelKeypad.Controls.Add(this.PinCodeTextBox);
            this.panelKeypad.Controls.Add(this.button14);
            this.panelKeypad.Controls.Add(this.button15);
            this.panelKeypad.Controls.Add(this.button8);
            this.panelKeypad.Controls.Add(this.button9);
            this.panelKeypad.Controls.Add(this.button10);
            this.panelKeypad.Controls.Add(this.button11);
            this.panelKeypad.Controls.Add(this.button7);
            this.panelKeypad.Controls.Add(this.button6);
            this.panelKeypad.Controls.Add(this.button5);
            this.panelKeypad.Controls.Add(this.button4);
            this.panelKeypad.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelKeypad.Location = new System.Drawing.Point(3, 453);
            this.panelKeypad.Name = "panelKeypad";
            this.panelKeypad.Size = new System.Drawing.Size(320, 310);
            this.panelKeypad.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(65, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 31);
            this.label1.TabIndex = 14;
            this.label1.Text = "Indtast pinkode";
            // 
            // LoginButton
            // 
            this.LoginButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LoginButton.AutoSize = true;
            this.LoginButton.BackColor = System.Drawing.Color.SteelBlue;
            this.LoginButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoginButton.Location = new System.Drawing.Point(166, 257);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(106, 50);
            this.LoginButton.TabIndex = 13;
            this.LoginButton.Text = "ENTER";
            this.LoginButton.UseVisualStyleBackColor = false;
            this.LoginButton.Click += new System.EventHandler(this.LoginButtonClick);
            // 
            // button12
            // 
            this.button12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button12.AutoSize = true;
            this.button12.BackColor = System.Drawing.Color.LightBlue;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(54, 257);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(106, 50);
            this.button12.TabIndex = 12;
            this.button12.Text = "DEL";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.ClearPinClick);
            // 
            // button13
            // 
            this.button13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button13.AutoSize = true;
            this.button13.BackColor = System.Drawing.Color.LightBlue;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(166, 201);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(106, 50);
            this.button13.TabIndex = 11;
            this.button13.Text = "C";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.DeleteLastEntryClick);
            // 
            // PinCodeTextBox
            // 
            this.PinCodeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PinCodeTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.PinCodeTextBox.Enabled = false;
            this.PinCodeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PinCodeTextBox.ForeColor = System.Drawing.Color.DodgerBlue;
            this.PinCodeTextBox.Location = new System.Drawing.Point(54, 45);
            this.PinCodeTextBox.Name = "PinCodeTextBox";
            this.PinCodeTextBox.PasswordChar = '*';
            this.PinCodeTextBox.Size = new System.Drawing.Size(218, 38);
            this.PinCodeTextBox.TabIndex = 0;
            // 
            // button14
            // 
            this.button14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button14.AutoSize = true;
            this.button14.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(110, 201);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(50, 50);
            this.button14.TabIndex = 10;
            this.button14.Text = "9";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.NumericButtonClick);
            // 
            // button15
            // 
            this.button15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button15.AutoSize = true;
            this.button15.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(54, 201);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(50, 50);
            this.button15.TabIndex = 9;
            this.button15.Text = "8";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.NumericButtonClick);
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button8.AutoSize = true;
            this.button8.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(222, 145);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(50, 50);
            this.button8.TabIndex = 8;
            this.button8.Text = "7";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.NumericButtonClick);
            // 
            // button9
            // 
            this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button9.AutoSize = true;
            this.button9.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(166, 145);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(50, 50);
            this.button9.TabIndex = 7;
            this.button9.Text = "6";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.NumericButtonClick);
            // 
            // button10
            // 
            this.button10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button10.AutoSize = true;
            this.button10.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(110, 145);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(50, 50);
            this.button10.TabIndex = 6;
            this.button10.Text = "5";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.NumericButtonClick);
            // 
            // button11
            // 
            this.button11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button11.AutoSize = true;
            this.button11.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(54, 145);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(50, 50);
            this.button11.TabIndex = 5;
            this.button11.Text = "4";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.NumericButtonClick);
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button7.AutoSize = true;
            this.button7.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(222, 89);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(50, 50);
            this.button7.TabIndex = 4;
            this.button7.Text = "3";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.NumericButtonClick);
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.AutoSize = true;
            this.button6.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(166, 89);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(50, 50);
            this.button6.TabIndex = 3;
            this.button6.Text = "2";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.NumericButtonClick);
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.AutoSize = true;
            this.button5.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(110, 89);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(50, 50);
            this.button5.TabIndex = 2;
            this.button5.Text = "1";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.NumericButtonClick);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.AutoSize = true;
            this.button4.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(54, 89);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 50);
            this.button4.TabIndex = 1;
            this.button4.Text = "0";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.NumericButtonClick);
            // 
            // StatusPanel2
            // 
            this.StatusPanel2.Controls.Add(this.msg2part3);
            this.StatusPanel2.Controls.Add(this.msg2part2);
            this.StatusPanel2.Controls.Add(this.msg2part1);
            this.StatusPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.StatusPanel2.Location = new System.Drawing.Point(3, 3);
            this.StatusPanel2.Name = "StatusPanel2";
            this.StatusPanel2.Size = new System.Drawing.Size(320, 144);
            this.StatusPanel2.TabIndex = 0;
            // 
            // msg2part3
            // 
            this.msg2part3.AutoSize = true;
            this.msg2part3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msg2part3.Location = new System.Drawing.Point(71, 83);
            this.msg2part3.Name = "msg2part3";
            this.msg2part3.Size = new System.Drawing.Size(118, 31);
            this.msg2part3.TabIndex = 21;
            this.msg2part3.Text = "scanner";
            // 
            // msg2part2
            // 
            this.msg2part2.AutoSize = true;
            this.msg2part2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msg2part2.Location = new System.Drawing.Point(71, 52);
            this.msg2part2.Name = "msg2part2";
            this.msg2part2.Size = new System.Drawing.Size(222, 31);
            this.msg2part2.TabIndex = 20;
            this.msg2part2.Text = "tommelfinger på";
            // 
            // msg2part1
            // 
            this.msg2part1.AutoSize = true;
            this.msg2part1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msg2part1.Location = new System.Drawing.Point(21, 21);
            this.msg2part1.Name = "msg2part1";
            this.msg2part1.Size = new System.Drawing.Size(146, 31);
            this.msg2part1.TabIndex = 19;
            this.msg2part1.Text = "2.   Placer";
            // 
            // StatusPanel3
            // 
            this.StatusPanel3.Controls.Add(this.Msg3part2);
            this.StatusPanel3.Controls.Add(this.Msg3part1);
            this.StatusPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StatusPanel3.Location = new System.Drawing.Point(3, 153);
            this.StatusPanel3.Name = "StatusPanel3";
            this.StatusPanel3.Size = new System.Drawing.Size(320, 144);
            this.StatusPanel3.TabIndex = 1;
            // 
            // Msg3part2
            // 
            this.Msg3part2.AutoSize = true;
            this.Msg3part2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Msg3part2.Location = new System.Drawing.Point(71, 45);
            this.Msg3part2.Name = "Msg3part2";
            this.Msg3part2.Size = new System.Drawing.Size(182, 31);
            this.Msg3part2.TabIndex = 25;
            this.Msg3part2.Text = "brug pinkode";
            // 
            // Msg3part1
            // 
            this.Msg3part1.AutoSize = true;
            this.Msg3part1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Msg3part1.Location = new System.Drawing.Point(21, 14);
            this.Msg3part1.Name = "Msg3part1";
            this.Msg3part1.Size = new System.Drawing.Size(248, 31);
            this.Msg3part1.TabIndex = 24;
            this.Msg3part1.Text = "2a. Ingen scanner";
            // 
            // NewUserPanel
            // 
            this.NewUserPanel.Controls.Add(this.label3);
            this.NewUserPanel.Controls.Add(this.EnrollButton);
            this.NewUserPanel.Controls.Add(this.EnrollLabel);
            this.NewUserPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NewUserPanel.Location = new System.Drawing.Point(3, 303);
            this.NewUserPanel.Name = "NewUserPanel";
            this.NewUserPanel.Size = new System.Drawing.Size(320, 144);
            this.NewUserPanel.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(290, 31);
            this.label3.TabIndex = 26;
            this.label3.Text = "2b. Ingen aftryk gemt";
            // 
            // EnrollButton
            // 
            this.EnrollButton.BackColor = System.Drawing.Color.LightBlue;
            this.EnrollButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.EnrollButton.Location = new System.Drawing.Point(100, 90);
            this.EnrollButton.Name = "EnrollButton";
            this.EnrollButton.Size = new System.Drawing.Size(106, 50);
            this.EnrollButton.TabIndex = 5;
            this.EnrollButton.Text = "Opret";
            this.EnrollButton.UseVisualStyleBackColor = false;
            this.EnrollButton.Click += new System.EventHandler(this.EnrollButtonClick);
            // 
            // EnrollLabel
            // 
            this.EnrollLabel.AutoSize = true;
            this.EnrollLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EnrollLabel.Location = new System.Drawing.Point(75, 47);
            this.EnrollLabel.Name = "EnrollLabel";
            this.EnrollLabel.Size = new System.Drawing.Size(181, 31);
            this.EnrollLabel.TabIndex = 4;
            this.EnrollLabel.Text = "tryk for opret";
            // 
            // authReLable
            // 
            this.authReLable.AutoSize = true;
            this.authReLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.authReLable.Location = new System.Drawing.Point(33, 319);
            this.authReLable.Name = "authReLable";
            this.authReLable.Size = new System.Drawing.Size(0, 31);
            this.authReLable.TabIndex = 21;
            // 
            // ThumbnailPanel
            // 
            this.ThumbnailPanel.AutoScroll = true;
            this.ThumbnailPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ThumbnailPanel.Location = new System.Drawing.Point(323, 141);
            this.ThumbnailPanel.Name = "ThumbnailPanel";
            this.ThumbnailPanel.Size = new System.Drawing.Size(634, 756);
            this.ThumbnailPanel.TabIndex = 17;
            // 
            // Msg1part1
            // 
            this.Msg1part1.AutoSize = true;
            this.Msg1part1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Msg1part1.Location = new System.Drawing.Point(3, 138);
            this.Msg1part1.Name = "Msg1part1";
            this.Msg1part1.Size = new System.Drawing.Size(207, 31);
            this.Msg1part1.TabIndex = 18;
            this.Msg1part1.Text = "1. Vælg billede";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ErrorMsgLabel);
            this.panel1.Controls.Add(this.StatusLabel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(1283, 141);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(314, 756);
            this.panel1.TabIndex = 19;
            // 
            // ErrorMsgLabel
            // 
            this.ErrorMsgLabel.AutoSize = true;
            this.ErrorMsgLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorMsgLabel.Location = new System.Drawing.Point(18, 65);
            this.ErrorMsgLabel.Name = "ErrorMsgLabel";
            this.ErrorMsgLabel.Size = new System.Drawing.Size(0, 31);
            this.ErrorMsgLabel.TabIndex = 1;
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusLabel.Location = new System.Drawing.Point(18, 21);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 31);
            this.StatusLabel.TabIndex = 0;
            // 
            // LoginUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1600, 900);
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LoginUI";
            this.Padding = new System.Windows.Forms.Padding(100);
            this.Text = "LoginUI App";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoginUI_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.HeaderPanel.ResumeLayout(false);
            this.HeaderPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panelKeypad.ResumeLayout(false);
            this.panelKeypad.PerformLayout();
            this.StatusPanel2.ResumeLayout(false);
            this.StatusPanel2.PerformLayout();
            this.StatusPanel3.ResumeLayout(false);
            this.StatusPanel3.PerformLayout();
            this.NewUserPanel.ResumeLayout(false);
            this.NewUserPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList userImageList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel HeaderPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.FlowLayoutPanel ThumbnailPanel;
        private System.Windows.Forms.Label authReLable;
        private System.Windows.Forms.Label Msg1part1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panelKeypad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button LoginButton;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.TextBox PinCodeTextBox;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel StatusPanel2;
        private System.Windows.Forms.Label msg2part3;
        private System.Windows.Forms.Label msg2part2;
        private System.Windows.Forms.Label msg2part1;
        private System.Windows.Forms.Panel StatusPanel3;
        private System.Windows.Forms.Label Msg3part2;
        private System.Windows.Forms.Label Msg3part1;
        private System.Windows.Forms.Panel NewUserPanel;
        private System.Windows.Forms.Button EnrollButton;
        private System.Windows.Forms.Label EnrollLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Label ErrorMsgLabel;
    }
}

