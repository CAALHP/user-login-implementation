﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CAALHP.Contracts;
using CAALHP.Events.Types;
using CAALHP.Events.UserServiceEvents;
//using FI.Events;
using crip2caalhp_events;
using FI.Events;
using FS.Events;
using Plugins.UserService.Managers;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using RequestTypes = FS.Events.RequestTypes;

namespace Plugins.UserService
{
    public class UserServiceImplementation : IServiceCAALHPContract
    {
        //caalhp classes
        private IServiceHostCAALHPContract _host;
        private int _processId;

        //user app classes
        private UserManager _userManager;

        private Dictionary<int, Guid> _userDictionary; 

        private AuthenticationManager _authManager;
        private UserProfile _user;
        private object _userLock = new object();
        //private UserProfile up;


        public UserServiceImplementation()
        {
           // Debugger.Launch();
            _userDictionary = new Dictionary<int, Guid>
            {
                {100, new Guid("5293218e-d930-4f91-97aa-cca42425a0b8")},
                {101, new Guid("d3e79f9d-0b50-4b8e-8145-407d4d371c61")},
                {200, new Guid("a0e2667b-1438-4403-adda-cb0c5f0acc4c")},
                {345, new Guid("74d6638d-ff32-4554-8fa2-f8981a386af0")}
            };
        }

        public UserProfile User
        {
            get
            {
                UserProfile res;
                lock (_userLock)
                {
                    res = _user;
                }
                return res;
            }
            private set
            {
                lock (_userLock)
                {
                    _user = value;
                }

            }
        }

        //handle "authentication request" and "authentication response" events
        private void HandleEvent(AuthenticationRequestEvent userCredentials)
        {
            switch (userCredentials.ReqType)
            {
                case CAALHP.Events.Types.RequestTypes.CheckforScanner:
                    CheckForScannerRequest();
                    break;
                case CAALHP.Events.Types.RequestTypes.Enroll:
                    RequestEnroll(userCredentials.Username);
                    break;
                case CAALHP.Events.Types.RequestTypes.AuthByFinger:
                    AuthByFingerPrint(userCredentials.Username);
                    break;
                case CAALHP.Events.Types.RequestTypes.AuthByPincode:
                    AuthByPin(userCredentials);
                    break;
            }
        }

        //handle uselist event 
        private void HandleEvent(UserListRequestEvent userList)
        {

            var users = _userManager.GetUserListFromFile();
            var photoRoot = _userManager.ImgDirectoryPath;

            var response = new UserListResponseEvent
                {
                    Users = users,
                    PhotoRoot = photoRoot
                };

            var serializedList = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedList);

        }

        private void HandleEvent(FSResponsEvent fsRespons)
        {
            var type = fsRespons.RequestType;
            var response = new AuthenticationResponseEvent
            {
                Username = fsRespons.UserId,
                Response = fsRespons.Result,
                Message = fsRespons.Message ?? ""
            };

            switch (type)
            {
                case RequestTypes.Auth:
                    response.ReqType = CAALHP.Events.Types.RequestTypes.AuthByFinger;
                    if (fsRespons.Result)
                        LoginUser(fsRespons.UserId);
                    break;
                case RequestTypes.Enroll:
                    response.ReqType = CAALHP.Events.Types.RequestTypes.Enroll;
                    break;
                case RequestTypes.CheckforScanner:
                    response.ReqType = CAALHP.Events.Types.RequestTypes.CheckforScanner;
                    break;
                case RequestTypes.DeleteUser:
                    throw new NotImplementedException();
                    break;
            }

            CreateAuthenticationResponsEvent(response);
        }

        private void HandleEvent(FacialAuthRequestEvent req)
        {
            var request = new IdentificationRequestEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, request, "FI.Events");
            _host.Host.ReportEvent(serializedResponse);
        }

        private void HandleEvent(IdentificationResponsEvent res)
        {
            var userId = res.UserId;

            LoginUser(userId);
        }

        private void HandleEvent(IdentificationErrorResponsEvent res)
        {
            var response = new FacialErrorEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                Message = res.Error
            };

            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedResponse);
        }

        private void HandleEvent(IdentificationFailedResponsEvent res)
        {

            var response = new FacialAuthFailedResponsEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedResponse);

        }

        private void HandleEvent(IsUserLoggedInRequestEvent res)
        {
            var response = new IsUserLoggedInResponsEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                User = _user,
                PhotoRoot = _userManager.ImgDirectoryPath
            };

            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedResponse);
        }

        public void HandleEvent(UserLoggedOutEvent res)
        {
            LogoutUser();
        }

        public void HandleEvent(FingerTemplateRequestEvent res)
        {
            var userId = res.UserId;
            var temlate = _userManager.GetFingerTemplate(userId);

            var respons = new FingerTemplateResponsEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                Template = temlate,
                UserId = userId
            };


            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, respons, "FS.Events");
            _host.Host.ReportEvent(serializedResponse);
        }

        public void HandleEvent(FacialTemplateRequestEvent res)
        {
            var userId = res.UserId;
            var temlate = _userManager.GetFacialTemplate(userId);

            var respons = new FacialTemplateResponsEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    Template = temlate,
                    UserId = userId
                };


            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, respons, "FI.Events");
            _host.Host.ReportEvent(serializedResponse);

        }

        public void HandleEvent(CreateUserLocalEvent usr)
        {
            var user = usr.UserProfile;
            var hasPhoto = usr.NewPhoto;
            var photoRoot = usr.PhotoRoot;

            _userManager.UpdateUser(user, hasPhoto, photoRoot);
        }

        public void HandleEvent(FoundIdFromCripEvent cripEvent)
        {

            try
            {
                var userId = _userDictionary[cripEvent.Id];
                LoginUser(userId.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        //notify events
        public void Notify(KeyValuePair<string, string> notification)
        {
            Console.WriteLine("Notify Called\n");

            //var serializer = EventHelper.GetSerializationTypeFromFullyQualifiedNameSpace(notification.Key);
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }


        #region CAALHP Conctact

        public string GetName()
        {
            return "UserService";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            //Debugger.Launch();

            Console.WriteLine("Initilized called \n");

            _host = hostObj;
            _processId = processId;

            _userManager = new UserManager(_host);
            _authManager = new AuthenticationManager(_host, _userManager);

            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(AuthenticationRequestEvent)),_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(CreateUserLocalEvent)),_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserListRequestEvent)),_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(IsUserLoggedInRequestEvent)),_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedOutEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FacialAuthRequestEvent)),_processId);_host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(IdentificationResponsEvent),"FI.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(IdentificationFailedResponsEvent),"FI.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(IdentificationErrorResponsEvent),"FI.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FacialTemplateRequestEvent),"FI.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FSResponsEvent), "FS.Events"),_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FingerTemplateRequestEvent), "FS.Events"),_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FoundIdFromCripEvent), "crip2caalhp-events"), _processId);
            //_userManager = new UserManager(_host);

            _userManager.TryUpdateFromServer();

            Console.WriteLine("Initilized finished \n");
        }
        
        //copy files from user service location to output folder
        
        public void Start()
        {

        }

        public void Stop()
        {

        }

        #endregion

        private void LogoutUser()
        {
            User = null;
        }

        private void LoginUser(string userId)
        {

            try
            {
                User = _userManager.GetUserListFromFile().Single(x => x.UserId == userId);
            }
            catch (Exception)
            {
                //todo lookinto raising propper event here.
                User = null;
                throw;
            }

            var response = new UserLoggedInEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    User = User,
                    PhotoRoot = _userManager.ImgDirectoryPath
                };

            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedResponse);
        }

        private void RequestEnroll(string userId)
        {
            var request = new FSRequestEvent
            {
                RequestType = RequestTypes.Enroll,
                UserId = userId,
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            CreateFingerScannerEvent(request);
        }

        private void CheckForScannerRequest()
        {
            var request = new FSRequestEvent
            {
                RequestType = RequestTypes.CheckforScanner,
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            CreateFingerScannerEvent(request);
        }

        private void AuthByFingerPrint(string userId)
        {
            var request = new FSRequestEvent
            {
                RequestType = RequestTypes.Auth,
                UserId = userId,
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            CreateFingerScannerEvent(request);
        }

        private void CreateFingerScannerEvent(FSRequestEvent request)
        {
            var serializedList = EventHelper.CreateEvent(SerializationType.Json, request, "FS.Events");
            _host.Host.ReportEvent(serializedList);
        }

        private void AuthByPin(AuthenticationRequestEvent userCredentials)
        {
            var userId = userCredentials.Username;

            var validationResult = _authManager.authenticateUser(userId, userCredentials.Pincode);

            var response = new AuthenticationResponseEvent
            {
                Username = userId,
                Response = validationResult,
                ReqType = CAALHP.Events.Types.RequestTypes.AuthByPincode


            };

            if (validationResult)
                LoginUser(userId);

            CreateAuthenticationResponsEvent(response);
        }

        private void CreateAuthenticationResponsEvent(AuthenticationResponseEvent response)
        {
            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedResponse);
        }

    }
}