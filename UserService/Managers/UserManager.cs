﻿using System.Diagnostics;
using System.Reflection;
using System.Timers;
using CAALHP.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Drawing;
using System.Configuration;
using CAALHP.Events;
using CAALHP.Events.Types;
using CAALHP.Events.UserServiceEvents;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CareStoreServiceContracts;
using Plugins.UserService.DataService;


namespace Plugins.UserService.Managers
{
    public class UserManager
    {
        #region fields

        private IServiceHostCAALHPContract _host;
        private IDataService _dataService;
        private string _assemblyPath;
        private string _userFileName;
        private bool _dataServiceEnabled;
        private Timer _updateTimer;
        private object _userFileLock = new object();
        private bool _updating;

        #endregion

        #region constructor

        public UserManager(IServiceHostCAALHPContract host)
        {
            //Debugger.Launch();
            _host = host;
            _userFileName = "Users.xml";
            _assemblyPath = AssemblyDirectory + "\\";

            //_updateTimer = new Timer(15000) { AutoReset = false };
            int updateinterval;
            try
            {
                updateinterval = Convert.ToInt32(ConfigurationManager.AppSettings.Get("updateinterval"));
                if(updateinterval < 100000)
                    updateinterval = 900000;
            }
            catch (Exception)
            {
                updateinterval = 900000;
            }
            
            _updateTimer = new Timer(updateinterval) { AutoReset = true };
            _updateTimer.Elapsed += UpdateTimerOnElapsed;
            _updateTimer.Start();

            try
            {
                _dataService = new DataService.DataService();
                _dataServiceEnabled = true;

            }
            catch (Exception e)
            {
                _dataService = null;
                _dataServiceEnabled = false;
                //throw e;
            }


            var dataDirectory = Path.Combine(_assemblyPath, "Data\\");
            if (!Directory.Exists(dataDirectory))
                Directory.CreateDirectory(dataDirectory);
           // CopyDefaultUsersXmlToPath(dataDirectory);

        }


        public UserManager()
        {

        }

        #endregion

        #region public methods

        public void TryUpdateFromServer()
        {
            try
            {
                if (_dataServiceEnabled)
                    UpdateUserListFromDataService();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public string UserFilePath
        {
            get { return Path.Combine(_assemblyPath, "Data\\", _userFileName); }
        }

        public string ImgDirectoryPath
        {
            get { return Path.Combine(_assemblyPath, "Images\\"); }
        }

        //get list of users in the xml file
        public List<UserProfile> GetUserListFromFile()
        {
            List<UserProfile> usrlst;

            try
            {
                lock (_userFileLock)
                {
                    //UserList usrlst = new UserList();
                    XmlSerializer deserializer = new XmlSerializer(typeof(List<UserProfile>));
                    TextReader textReader = new StreamReader(UserFilePath, true);
                    //var usrlst = deserializer.Deserialize(textReader) as List<UserProfile>;
                    usrlst = deserializer.Deserialize(textReader) as List<UserProfile>;
                    textReader.Close();
                }
            }
            catch (Exception)
            {
                
               usrlst = new List<UserProfile>();
            }

            usrlst.RemoveAll(item => item == null);

            CheckForImg(usrlst);

            return usrlst;
        }

        public byte[] GetFingerTemplate(string id)
        {
            byte[] templateToReturn = null;
            var dirPath = _assemblyPath + "fingerTemplates\\";
            var sourcePath = Path.Combine(dirPath, id + ".dat");

            if (File.Exists(sourcePath))
                templateToReturn = File.ReadAllBytes(sourcePath);

            return templateToReturn;
        }

        public byte[] GetFacialTemplate(string id)
        {
            byte[] templateToReturn = null;

            var dirPath = _assemblyPath + "facialTemplates\\";
            var sourcePath = Path.Combine(dirPath, id + ".dat");

            if (File.Exists(sourcePath))
                templateToReturn = File.ReadAllBytes(sourcePath);

            return templateToReturn;
        }

        public void UpdateUser(UserProfile user, bool hasPhoto, string photoRoot)
        {
            //TODO wrap some updateing thing here, to avoid threading issue.
            List<UserProfile> localList;
            UserProfile userToBeReplaced;

            try
            {
                if (hasPhoto)
                    user.HasPhoto = CopyImgFromSource(user.PhotoLink, photoRoot);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                user.HasPhoto = false;
            }

            try
            {

                localList = GetUserListFromFile();

                //try
                //{
                //    userToBeReplaced = localList.Single(x => x.UserId == user.UserId);
                //}
                //catch (Exception)
                //{
                //    userToBeReplaced = null;
                //}
                
                //if (userToBeReplaced != null)
                //{
                //    localList.Remove(userToBeReplaced);
                //    localList.Add(user);
                //}
                //else
                //    localList.Add(user);
                var oldUser = localList.FirstOrDefault(x => x.UserId == user.UserId);
                if (oldUser != null)
                {
                    int index = localList.IndexOf(oldUser);
                    localList[index] = user;
                }
                else
                {
                    localList.Add(user);
                }
                
                //WriteUserListToFile(localList);


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                localList = new List<UserProfile>();
                localList.Add(user);
                //WriteUserListToFile(localList);

            }

            WriteUserListToFile(localList);

        }

        #endregion

        #region private methods

        private bool CopyImgFromSource(string photoLink, string photoRoot)
        {
            var dirPath = _assemblyPath + "Images\\";
            var imagePath = Path.Combine(dirPath, photoLink);

            //var targetImgPath = Path.Combine(ImagePath, photoLink);
            var sourceImgPath = Path.Combine(photoRoot, photoLink);
            if (!File.Exists(imagePath)) return false;

            if (!File.Exists(imagePath) || File.GetLastWriteTime(imagePath) < File.GetLastWriteTime(sourceImgPath))
            {
                File.Copy(sourceImgPath, imagePath, true);
            }

            return true;
        }

        private void WriteUserListToFile(List<UserProfile> uList)
        {

            var dirPath = Path.Combine(_assemblyPath, "Data\\");

            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            lock (_userFileLock)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<UserProfile>));

                FileStream fs = new FileStream(UserFilePath, FileMode.Create);
                TextWriter writer = new StreamWriter(fs, new UTF8Encoding());
                // Serialize using the XmlTextWriter.
                serializer.Serialize(writer, uList);
                writer.Close();
            }

        }


        private void UpdateTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            //Debugger.Launch();
            Console.WriteLine("Updatetimer Elapsed");

            if (_dataServiceEnabled && !_updating)
                Task.Run(() => UpdateUserListFromDataService());
            else if (!_dataServiceEnabled && !_updating)
            {
                try
                {
                    _dataService = new DataService.DataService();
                    _dataServiceEnabled = true;

                    Task.Run(() => UpdateUserListFromDataService());

                }
                catch (Exception e)
                {
                    _dataService = null;
                    _dataServiceEnabled = false;
                    //throw e;
                }
            }
        }

        private async void UpdateUserListFromDataService()
        {
            _updating = true;
            List<UserProfile> newLocalList = new List<UserProfile>();
            List<UserProfileDataObject> updatedUserList = null;

            try
            {
                updatedUserList = _dataService.GetUserList();
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to update UserList form server \n");
                Console.WriteLine(e.Message);
                _dataServiceEnabled = false;
                updatedUserList = null;
                _updating = false;
            }
            
            if (updatedUserList == null)
            {
                _updating = false;
                return;
            }

            var localList = GetUserListFromFile();
            if (localList == null)
                localList = new List<UserProfile>();

            await Task.Run(() => Parallel.ForEach(updatedUserList, entity =>
                {
                    //todo consider doing something about username.
                    //TODO refactor into factory
                    UserProfile updatedUser = new UserProfile()
                    {
                        CreationTime = entity.CreationTime,
                        DateOfBirth = entity.DateOfBirth,
                        Email = entity.Email,
                        Height = entity.Height,
                        UserId = entity.Id,
                        MobileNumber = entity.MobileNumber,
                        Pin = entity.Pin,
                        FullName = entity.Name,
                        FacialProfile = entity.HasFacialProfileTemplate,
                        FingerPrint = entity.HasFingerPrintTemplate,
                        HasPhoto = entity.HasProfileImage,
                        UserLevel = entity.UserLevel == CareStoreUserLevel.Citizen.ToString() ? CareStoreUserLevel.Citizen : CareStoreUserLevel.Caretaker,
                        UserName = entity.Name
                    };



                    //UserProfileDataObject user = entity;
                    UserProfile result = (from a in localList where a.UserId == updatedUser.UserId select a).SingleOrDefault();
                    if (result != null)
                        CheckForUpdates(updatedUser);
                    else
                        CopyTemplatesFromServer(updatedUser);

                    newLocalList.Add(updatedUser);
                }));

            //foreach (var entity in updatedUserList)
            //{
            //    //todo consider doing something about username.
            //    //TODO refactor into factory
            //    UserProfile updatedUser = new UserProfile()
            //    {
            //        CreationTime = entity.CreationTime,
            //        DateOfBirth = entity.DateOfBirth,
            //        Email = entity.Email,
            //        Height = entity.Height,
            //        UserId = entity.Id,
            //        MobileNumber = entity.MobileNumber,
            //        Pin = entity.Pin,
            //        FullName = entity.Name,
            //        FacialProfile = entity.HasFacialProfileTemplate,
            //        FingerPrint = entity.HasFingerPrintTemplate,
            //        HasPhoto = entity.HasProfileImage,
            //        UserLevel = entity.UserLevel == CareStoreUserLevel.Citizen.ToString() ? CareStoreUserLevel.Citizen : CareStoreUserLevel.Caretaker,
            //        UserName = entity.Name
            //    };



            //    //UserProfileDataObject user = entity;
            //    UserProfile result = (from a in localList where a.UserId == updatedUser.UserId select a).SingleOrDefault();
            //    if (result != null)
            //        CheckForUpdates(result, updatedUser);
            //    else
            //        GetTemplatesFromServer(updatedUser);

            //    newLocalList.Add(updatedUser);
            //}

            try
            {
                WriteUserListToFile(newLocalList);
            }
            catch (Exception)
            {
                _updating = false;
               // throw;
            }

            _updating = false;

            ReportUserListUpdate(newLocalList);

        }

        private void ReportUserListUpdate(List<UserProfile> newLocalList)
        {
            var users = newLocalList;

            var response = new UserUpdateCompleteEvent
            {
                UserList = users,
            };

            var serializedList = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedList);
        }

        private void CopyTemplatesFromServer(UserProfile user)
        {
            if (user.HasPhoto)
                Task.Run(() => CopyImgFromServer(user));
            if (user.FingerPrint)
                Task.Run(() => CopyFingerprintTemplateFromServer(user));
            if (user.FacialProfile)
                Task.Run(() => CopyFacialTemplateFromServer(user));
        }

        private void CheckForUpdates(UserProfile updatedUser)
        {
            var imagePath = _assemblyPath + "Images\\" + updatedUser.UserId + ".dat";
            var facielPath = _assemblyPath + "facialTemplates\\" + updatedUser.UserId + ".dat";
            var fingerPath = _assemblyPath + "fingerTemplates\\" + updatedUser.UserId + ".dat";

            //if (localUser.HasPhoto == false && updatedUser.HasPhoto)
            //    Task.Run(() => CopyImgFromServer(localUser));
            //if (localUser.FingerPrint == false && updatedUser.FingerPrint )
            //    Task.Run(() => CopyFingerprintTemplateFromServer(updatedUser.UserId));
            //if (localUser.FacialProfile == false && updatedUser.FacialProfile)
            //    Task.Run(() => CopyFacialTemplateFromServer(updatedUser.UserId));
            if (updatedUser.HasPhoto && !File.Exists(imagePath))
                Task.Run(() => CopyImgFromServer(updatedUser));
            if (updatedUser.FingerPrint && !File.Exists(fingerPath))
                Task.Run(() => CopyFingerprintTemplateFromServer(updatedUser));
            if (updatedUser.FacialProfile && !File.Exists(facielPath))
                Task.Run(() => CopyFacialTemplateFromServer(updatedUser));

        }

        private void CopyFacialTemplateFromServer(UserProfile user)
        {
            byte[] facialtemplate;
            var dirPath = _assemblyPath + "facialTemplates\\";
            var targetPath = Path.Combine(dirPath, user.UserId + ".dat");

            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            try
            {
                facialtemplate = _dataService.GetFacialTemplate(user.UserId);

                File.WriteAllBytes(targetPath, facialtemplate);
            }
            catch (Exception e)
            {
                //TODO if this fails set hasfingerprint to false. 
               // throw;
              //  user.FacialProfile = false;
            }

            //todo add a event to enroll finger print, also set to false if failed.
        }

        private void CopyFingerprintTemplateFromServer(UserProfile user)
        {
            byte[] fingertemplate;
            var dirPath = _assemblyPath + "fingerTemplates\\";
            var targetPath = Path.Combine(dirPath, user.UserId + ".dat");

            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);
            
            try
            {
                fingertemplate = _dataService.GetFingerTemplate(user.UserId);
                File.WriteAllBytes(targetPath, fingertemplate);
            }
            catch (Exception)
            {
                //TODO if this fails set hasfingerprint to false. 
                //throw;
               // user.FingerPrint = false;
            }

            //todo add a event to enroll finger print, also set to false if failed.

        }

        private void CopyImgFromServer(UserProfile user)
        {
            var dirPath = _assemblyPath + "Images\\";
            var imagePath = Path.Combine(dirPath, user.PhotoLink);
            try
            {
                byte[] imgArray = _dataService.GetProfilePicture(user.UserId);

                File.WriteAllBytes(imagePath, imgArray);
            }
            catch (Exception)
            {
                //throw;
               // user.HasPhoto = false;
            }

        }

        private void CopyDefaultUsersXmlToPath(string outPutDirectory)
        {
            var defaultFilePath = Path.Combine(_assemblyPath, _userFileName);
            var outPutFilePath = Path.Combine(outPutDirectory, _userFileName);

            if (File.Exists(outPutFilePath))
                return;

            if (!Directory.Exists(outPutDirectory))
                Directory.CreateDirectory(outPutDirectory);

            if (!File.Exists(defaultFilePath))
                CreateDefaultXmlFile(defaultFilePath);

            File.Copy(defaultFilePath, outPutFilePath);
        }

        private void CreateDefaultXmlFile(string path)
        {
            var userList = new List<UserProfile>();
            var defaultUser = new UserProfile();

            userList.Add(defaultUser);

            XmlSerializer serializer = new XmlSerializer(typeof(List<UserProfile>));

            FileStream fs = new FileStream(path, FileMode.Create);
            TextWriter writer = new StreamWriter(fs, new UTF8Encoding());
            // Serialize using the XmlTextWriter.
            serializer.Serialize(writer, userList);
            writer.Close();
        }



        //Checks if the image exsists in local folder, or sets photolink to null if it doest
        private void CheckForImg(List<UserProfile> users)
        {

            var sourcePath = _assemblyPath + "Images\\";

            if (!Directory.Exists(sourcePath))
            {
                Directory.CreateDirectory(sourcePath);
            }

            foreach (var user in users)
            {

                if (user == null) continue;
                try
                {
                    if ( user.PhotoLink == null) continue;

                    var current = AssemblyDirectory;
                    if (current == null)
                    {
                        user.HasPhoto = false;
                        continue;
                    }

                    var sourceImagePath = Path.Combine(sourcePath, user.PhotoLink);
                    //if (!File.Exists(sourceImagePath))
                    //    user.PhotoLink = null;
                    user.HasPhoto = File.Exists(sourceImagePath);
                }
                catch (Exception)
                {

                    user.HasPhoto = false;
                }

            }

        }

        private static string AssemblyDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        #endregion


    }
}