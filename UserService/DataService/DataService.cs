﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using CareStoreServiceContracts;

namespace Plugins.UserService.DataService
{
    public class DataService: IDataService
    {

        private ICareStoreMarketplaceContract _market;

        public DataService()
        {
            

            try
            {
                //var maxsize = 2147483648;
                //binding
                var binding = new BasicHttpBinding();
                binding.MaxBufferSize = 2138483646;
                binding.MaxReceivedMessageSize = 2138483646;
                binding.SendTimeout = new System.TimeSpan(1, 0, 0, 0);
                binding.ReceiveTimeout = new System.TimeSpan(1, 0, 0, 0);
                //var binding = new BasicHttpsBinding();
                //endpoint
                var endpoint = new EndpointAddress(ConfigurationManager.AppSettings.Get("marketplaceAddress"));
                //<add key="marketplaceAddress" value="http://192.168.1.100/Marketplaceservice/CareStoreMarketplaceWcfService.svc"/>
                //var endpoint = "http://192.168.1.100/Marketplaceservice/CareStoreMarketplaceWcfService.svc";
               // var endpoint = "http://192.168.1.8/Marketplaceservice/CareStoreMarketplaceWcfService.svc";
                //channelfactory
                var channelFactory = new ChannelFactory<ICareStoreMarketplaceContract>(binding, endpoint);
                //System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                //    (sender, certificate, chain, sslPolicyErrors) => true;
                _market = channelFactory.CreateChannel();
            }
            catch (Exception e)
            {

                throw;
            }
            
        }
        
        public List<UserProfileDataObject> GetUserList()
        {
            return _market.GetUsers();
        }

        public byte[] GetFingerTemplate(string id)
        {
            return _market.GetFingerTemplate(id);
        }

        public byte[] GetFacialTemplate(string id)
        {
            return _market.GetFacialTemplate(id);
        }

        public byte[] GetProfilePicture(string id)
        {
            return _market.GetProfilePicture(id);
        }
    }
}
