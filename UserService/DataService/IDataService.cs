﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareStoreServiceContracts;

namespace Plugins.UserService.DataService
{
    public interface IDataService
    {
        List<UserProfileDataObject> GetUserList();
        byte[] GetFingerTemplate(string id);
        byte[] GetFacialTemplate(string id);
        byte[] GetProfilePicture(string id);
    }
}
